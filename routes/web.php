<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
//    return view('welcome');
    return redirect()->route('login');
});

/*Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});*/

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes

    Route::group(['prefix' => 'role'], function () {
        Route::get('/', 'RoleController@index')->name('role');
        Route::post('/', 'RoleController@store')->name('role.store');
        Route::get('/edit/{id}', 'RoleController@edit')->name('role.edit');
        Route::get('/create', 'RoleController@create')->name('role.create');
        Route::get('/show/{id}', 'RoleController@show')->name('role.show');
        Route::get('/data', 'RoleController@data')->name('role.data');
        Route::put('/update/{id?}', 'RoleController@update')->name('role.update');
        Route::get('/delete/{id}', 'RoleController@destroy')->name('role.delete');
    });

    Route::group(['prefix' => 'permission'], function () {
        Route::get('/', 'PermissionsController@index')->name('permission');
        Route::post('/', 'PermissionsController@store')->name('permission.store');
        Route::get('/edit/{id}', 'PermissionsController@edit')->name('permission.edit');
        Route::get('/create', 'PermissionsController@create')->name('permission.create');
        Route::get('/show/{id}', 'PermissionsController@show')->name('permission.show');
        Route::get('/data', 'PermissionsController@data')->name('permission.data');
        Route::put('/update/{id}', 'PermissionsController@update')->name('permission.update');
        Route::get('/delete/{id}', 'PermissionsController@destroy')->name('permission.delete');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index')->name('user');
        Route::post('/', 'UserController@store')->name('user.store');
        Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
        Route::get('/create', 'UserController@create')->name('user.create');
        Route::get('/show/{id}', 'UserController@show')->name('user.show');
        Route::get('/data', 'UserController@data')->name('user.data');
        Route::put('/update/{id}', 'UserController@update')->name('user.update');
        Route::get('/delete/{id}', 'UserController@destroy')->name('user.delete');
    });

    Route::group(['prefix' => 'employee'], function () {
        Route::get('/', 'EmployeeController@index')->name('employee');
        Route::post('/', 'EmployeeController@store')->name('employee.store');
        Route::get('/edit/{id}', 'EmployeeController@edit')->name('employee.edit');
        Route::get('/create', 'EmployeeController@create')->name('employee.create');
        Route::get('/show/{id}', 'EmployeeController@show')->name('employee.show');
        Route::get('/data', 'EmployeeController@data')->name('employee.data');
        Route::put('/update/{id}', 'EmployeeController@update')->name('employee.update');
        Route::get('/delete/{id}', 'EmployeeController@destroy')->name('employee.delete');
        Route::get('/get-employee', 'EmployeeController@getListEmployee')->name('employee.get_list');
    });

    Route::group(['prefix' => 'disposition'], function () {
        Route::get('/', 'DispositionController@index')->name('disposition');
        Route::post('/', 'DispositionController@store')->name('disposition.store');
        Route::get('/edit/{id}', 'DispositionController@edit')->name('disposition.edit');
        Route::get('/create', 'DispositionController@create')->name('disposition.create');
        Route::get('/show/{id}', 'DispositionController@show')->name('disposition.show');
        Route::get('/pdf/{id}', 'DispositionController@downlo32adPdf')->name('disposition.pdf');
        Route::get('/detail/{id}', 'DispositionController@detail')->name('disposition.detail');
        Route::get('/response/{id}', 'DispositionController@responseLetter')->name('disposition.response');
        Route::post('/response/{id}', 'DispositionController@storeResponse')->name('disposition.response.store');
        Route::get('/data', 'DispositionController@data')->name('disposition.data');
        Route::put('/update/{id}', 'DispositionController@update')->name('disposition.update');
        Route::get('/delete/{id}', 'DispositionController@destroy')->name('disposition.delete');
    });

    Route::group(['prefix' => 'telaah-staf'], function () {
        Route::get('/', 'TelaahStafController@index')->name('telaah-staf');
        Route::post('/', 'TelaahStafController@store')->name('telaah-staf.store');
        Route::get('/edit/{id}', 'TelaahStafController@edit')->name('telaah-staf.edit');
        Route::get('/create', 'TelaahStafController@create')->name('telaah-staf.create');
        Route::get('/show/{id}', 'TelaahStafController@show')->name('telaah-staf.show');
        Route::get('/pdf/{id}', 'TelaahStafController@downloadPdf')->name('telaah-staf.pdf');
        Route::get('/detail/{id}', 'TelaahStafController@detail')->name('telaah-staf.detail');
        Route::get('/data', 'TelaahStafController@data')->name('telaah-staf.data');
        Route::put('/update/{id}', 'TelaahStafController@update')->name('telaah-staf.update');
        Route::get('/delete/{id}', 'TelaahStafController@destroy')->name('telaah-staf.delete');
    });

    Route::group(['prefix' => 'spt'], function () {
        Route::get('/', 'SptController@index')->name('spt');
        Route::post('/', 'SptController@store')->name('spt.store');
        Route::get('/edit/{id}', 'SptController@edit')->name('spt.edit');
        Route::get('/create', 'SptController@create')->name('spt.create');
        Route::get('/show/{id}', 'SptController@show')->name('spt.show');
        Route::get('/pdf/{id}', 'SptController@downloadPdf')->name('spt.pdf');
        Route::get('/detail/{id}', 'SptController@detail')->name('spt.detail');
        Route::get('/data', 'SptController@data')->name('spt.data');
        Route::put('/update/{id}', 'SptController@update')->name('spt.update');
        Route::get('/delete/{id}', 'SptController@destroy')->name('spt.delete');
    });

    Route::group(['prefix' => 'sppd'], function () {
        Route::get('/', 'SppdController@index')->name('sppd');
        Route::post('/{id}', 'SppdController@store')->name('sppd.store');
        Route::get('/edit/{id}', 'SppdController@edit')->name('sppd.edit');
        Route::get('/create/{id}', 'SppdController@create')->name('sppd.create');
        Route::get('/show/{id}', 'SppdController@show')->name('sppd.show');
        Route::get('/pdf/{id}', 'SppdController@downloadPdf')->name('sppd.pdf');
        Route::get('/detail/{id}', 'SppdController@detail')->name('sppd.detail');
        Route::get('/data', 'SppdController@data')->name('sppd.data');
        Route::put('/update/{id}', 'SppdController@update')->name('sppd.update');
        Route::get('/delete/{id}', 'SppdController@destroy')->name('sppd.delete');
    });

    Route::group(['prefix' => 'position'], function () {
        Route::get('/get-position', 'PositionController@getPosition')->name('position.get_list');
    });

    Route::group(['prefix' => 'grade'], function () {
        Route::get('/get-grade', 'GradeController@getDegree')->name('grade.get_list');
    });

    Route::group(['prefix' => 'setting'], function () {
        Route::get('/', 'SettingController@index')->name('setting');
        Route::post('/', 'SettingController@store')->name('setting.store');
    });
});
