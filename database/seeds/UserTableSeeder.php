<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new \App\Models\Permission();
        $permission->name="user";
        $permission->description="Akses Halaman User";
        $permission->save();

        $permission = new \App\Models\Permission();
        $permission->name="employee";
        $permission->description="Akses Halaman Pegawai";
        $permission->save();

        $permission = new \App\Models\Permission();
        $permission->name="setting";
        $permission->description="Akses Halaman Pengaturan";
        $permission->save();

        $permission = new \App\Models\Permission();
        $permission->name="disposition";
        $permission->description="Akses Halaman Surat Disposisi";
        $permission->save();

        $permission = new \App\Models\Permission();
        $permission->name="spt_sppd";
        $permission->description="Akses Halaman Surat SPT dan SPPD";
        $permission->save();

        $permission = new \App\Models\Permission();
        $permission->name="telaah_staff";
        $permission->description="Akses Halaman Surat Telaah Staff";
        $permission->save();

        $role = new Role();
        $role->name = 'Admin';
        $role->save();

        $role->addPermission('user');
        $role->addPermission('employee');
        $role->addPermission('setting');
        $role->addPermission('disposition');
        $role->addPermission('spt_sppd');
        $role->addPermission('telaah_staff');

        $user = new \App\User();
        $user->name ='Admin';
        $user->email = 'admin@gmail.com';
        $user->password = bcrypt('12345678');
        $user->save();
        $user->assignRole('Admin');

        $employee = new \App\Models\Employee();
        $employee->name ="Admin";
        $employee->email = 'gifary.upwork@gmail.com';
        $employee->nip ="12152400";
        $employee->phone_number = '08382139132';
        $employee->position = 'Admin';
        $employee->grade = 'admin';
        $employee->user_id = $user->id;
        $employee->save();


        $setting = new \App\Models\Setting();
        $setting->name="Sekretaris";
        $setting->code="sekretaris";

        $setting->save();

        $position1 = new \App\Models\Position();
        $position1->name = 'Sekretaris';
        $position1->save();


        $setting = new \App\Models\Setting();
        $setting->name="Kabid Bina Marga";
        $setting->code="kabid_bina_marga";
        $setting->save();


        $position2 = new \App\Models\Position();
        $position2->name = 'Kabid Bina Marga';
        $position2->parent_id = $position1->id;
        $position2->save();

        $setting = new \App\Models\Setting();
        $setting->name="Kabid Cipta Karya";
        $setting->code="kabid_cipta_karya";
        $setting->save();

        $position3 = new \App\Models\Position();
        $position3->name = 'Kabid Cipta Karya';
        $position3->parent_id = $position2->id;
        $position3->save();

        $setting = new \App\Models\Setting();
        $setting->name="Kabid Sumber Daya Air";
        $setting->code="kabid_sumber_daya_air";
        $setting->save();

        $position4 = new \App\Models\Position();
        $position4->name = 'Kabid Sumber Daya Air';
        $position4->parent_id = $position3->id;
        $position4->save();

        $setting = new \App\Models\Setting();
        $setting->name="Kabid Tata Ruang Dan Pertahanan";
        $setting->code="kabid_tata_ruang_dan_pertahanan";
        $setting->save();

        $position5 = new \App\Models\Position();
        $position5->name = 'Kabid Tata Ruang Dan Pertahanan';
        $position5->parent_id = $position4->id;
        $position5->save();

        $setting = new \App\Models\Setting();
        $setting->name="Kabid Perumahan";
        $setting->code="kabid_perumahan";
        $setting->save();

        $position6 = new \App\Models\Position();
        $position6->name = 'Kabid Perumahan';
        $position6->parent_id = $position5->id;
        $position6->save();

        $setting = new \App\Models\Setting();
        $setting->name="Kabid Bina Jasa Kontruksi";
        $setting->code="kabid_bina_jasa_kontruksi";
        $setting->save();

        $position7 = new \App\Models\Position();
        $position7->name = 'Kabid Bina Jasa Kontruksi';
        $position7->parent_id = $position6->id;
        $position7->save();

        $setting = new \App\Models\Setting();
        $setting->name="Kasubbag Perencanaan";
        $setting->code="kasubbag_perencanaan";
        $setting->save();

        $position8 = new \App\Models\Position();
        $position8->name = 'Kasubbag Perencanaan';
        $position8->parent_id = $position7->id;
        $position8->save();

        for($i=1;$i<=8;$i++){
            $employee = new \App\Models\Employee();
            $employee->name ="Pegawai ke-".$i;
            $employee->email = 'gifary.upwork@gmail.com';
            $employee->nip ="12152400".$i;
            $employee->phone_number = '08382139132'.$i;
            $employee->position = 'Admin';
            $employee->grade = 'eselon1';
            $employee->save();

        }

    }
}
