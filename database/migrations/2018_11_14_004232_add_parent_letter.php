<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentLetter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letters', function (Blueprint $table) {
            $table->string('parent_id',64)->after('id')->nullable();
            $table->string('pejabat_berwenang',255)->nullable();
            $table->string('pembebanan_anggaran',255)->after('pembebanan_anggaran_kode_rekening')->nullable();
            $table->string('alat_angkutan',64)->default('Taxi Darat, laut dan Udara');
            $table->string('nama_pengikut',255)->nullable();
            $table->string('nip_atau_umur_pengikut',64)->nullable();
            $table->string('hubungan_pengikut',64)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letters', function (Blueprint $table) {
            $table->dropColumn(['parent_id','pejabat_berwenang','alat_angkutan','nama_pengikut','nip_atau_umur_pengikut','hubungan_pengikut','pembebanan_anggaran']);
        });
    }
}
