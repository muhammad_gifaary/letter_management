<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letters', function (Blueprint $table) {
            $table->string('id', 64)->primary();
            $table->enum('type',['disposition','spt','sppd','telaah']);
            $table->string('letter_no',128);
            $table->string('no_agenda',128)->nullable();
            $table->string('dari')->nullable();
            $table->string('kepada')->nullable();
            $table->date('tanggal_surat')->nullable();
            $table->date('tanggal_diterima')->nullable();
            $table->string('sifat',64)->nullable();
            $table->text('perihal')->nullable();
            $table->text('catatan')->nullable();
            $table->text('diterusakan_kepada')->nullable();
            $table->text('dengan_hormat_harap')->nullable();
            $table->text('maksud_perjalanan_dinas')->nullable();
            $table->text('tujuan')->nullable();
            $table->date('tgl_berangkat')->nullable();
            $table->date('tgl_kembali')->nullable();
            $table->string('ditetapkan_di',128)->nullable();
            $table->date('tgl_penetapan')->nullable();
            $table->text('tembusan')->nullable();
            $table->text('pokok_persoalan')->nullable();
            $table->text('pra_anggapan')->nullable();
            $table->text('fakta_dan_data_yang_berpengaruh')->nullable();
            $table->text('kesimpulan')->nullable();
            $table->text('saran')->nullable();
            $table->boolean('persetujuan')->nullable();
            $table->string('attachment',64)->nullable();
            $table->string('diteruskan_kepada_custom')->nullable();

            $table->integer('employee_id_penetap')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letters');
    }
}
