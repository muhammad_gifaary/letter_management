<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterEmployeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('letter_id',64)->index();
            $table->unsignedInteger('employee_id')->index();
            $table->string('name',128);
            $table->string('nip',64);
            $table->string('position',64);
            $table->string('grade',64);
            $table->boolean('status_accept')->default(true)->nullable();
            $table->string('reason',255)->nullable();
            $table->timestamps();

            $table->foreign('letter_id')->references('id')->on('letters')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_employees');
    }
}
