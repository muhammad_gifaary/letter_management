<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letters', function (Blueprint $table) {
            $table->string('tempat_berangkat',255)->after('tgl_berangkat')->nullable();
            $table->string('tempat_tujuan',255)->after('tgl_berangkat')->nullable();
            $table->integer('lamanya')->after('tgl_berangkat')->nullable();
            $table->text('beban_biaya')->after('tgl_berangkat')->nullable();
            $table->text('untuk')->after('tgl_berangkat')->nullable();
            $table->text('catatan_lain_lain')->after('tgl_berangkat')->nullable();
            $table->text('perhatian')->after('tgl_berangkat')->nullable();
            $table->text('keterangan')->after('tgl_berangkat')->nullable();
            $table->string('pembebanan_anggaran_instansi',255)->after('tgl_berangkat')->nullable();
            $table->string('pembebanan_anggaran_kode_rekening',255)->after('tgl_berangkat')->nullable();
            $table->string('kode_nomor',255)->after('tgl_berangkat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letters', function (Blueprint $table) {
            $table->dropColumn(['tempat_berangkat','tempat_tujuan','lamanya','beban_biaya','untuk','catatan_lain_lain','perhatian',
                'keterangan','pemebebanan_anggaran_instansi','pemebebanan_anggaran_kode_rekening','kode_nomor','nomor_sppd']);
        });
    }
}
