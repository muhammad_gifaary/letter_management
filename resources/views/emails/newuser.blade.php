@component('mail::message')
    # Hello {{ $user->name }}

    <p>Selamat bergabung pada sistem HRMS. Berkut detail akun yang dapat digunakan untuk login</p>
    @component('mail::table')
        |        |          |
        | --------------------	|-------------|
        | Username      		| {{ $user->email }} atau {{ $user->username }}      |
        | Password      		| {{ $password }}      |
    @endcomponent

    <p>Silahkan login terlebih dahulu</p>
    @component('mail::button', ['url' => $action_url_login, 'color' => 'green'])
        LOGIN
    @endcomponent
    <p>Jika terdapat masalah dalam login silahkan lakukan reset password</p>
    @component('mail::button', ['url' => $action_url_reset, 'color' => 'red'])
        RESET PASSWORD
    @endcomponent

    <p>Jika ada pertanyaan atau ketidak sesuaian data silahkan e-mail ke gifary@sas-hospitality.com dengan mencantumkan Unit Kerja, nama lengkap, serta data yang harus diperbaikinya</p>

@endcomponent
