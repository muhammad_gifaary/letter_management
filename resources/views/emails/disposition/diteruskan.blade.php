@component('mail::message')
# Surat Disposisi baru

Terdapat surat baru dengan nomor surat {{ $letter->letter_no  }}

@component('mail::button', ['url' => $url_edit])
    Lihat secara detail surat
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
