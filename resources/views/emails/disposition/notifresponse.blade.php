@component('mail::message')
# Response surat disposisi
Terdapat response dengan nomor surat {{ $letterEmployee->letter->letter_no  }}
@component('mail::table')
| Nama       | Menyetujui         | Alasan  |
| ------------- |:-------------:| --------:|
| {{ $letterEmployee->name  }} ({{ $letterEmployee->position  }})     | @if($letterEmployee->status_accept) Disetujui @else Ditolak @endif      | {!! $letterEmployee->reason !!}      |
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent
