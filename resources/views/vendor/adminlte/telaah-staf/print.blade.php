<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> SmartMailPU </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('/css/all.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        p,h3 {
            margin: 0px;
            padding: 0px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <div class="row">
            <div class="text-center">
                <h3>TELAAH STAF</h3>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-xs-12 table-responsive">
                <table class="table">
                    <tr>
                        <td>Kepada YTH</td>
                        <td>{{$letter->kepada}}</td>
                    </tr>
                    <tr>
                        <td>Dari</td>
                        <td>{{$letter->dari}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>{{LetterHelper::convertDateToReadableFormat($letter->tanggal_surat)}}</td>
                    </tr>
                    <tr>
                        <td>Nomor</td>
                        <td>{{$letter->letter_no}}</td>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td>{{$letter->perihal}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td style="width: 10%;" class="text-center"><b>KOLOM DISPOSISI</b></td>
                        <td class="text-center" style="width: 90%"><b>ISI TELAAHAN</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align: center; horiz-align: center">
                            @foreach(array('1'=>'Setuju','0'=>'Tidak Setuju') as $key => $value)
                                @if($letter->persetujuan==$key)
                                    <p><input type="checkbox" checked disabled> {{$value}}</p>
                                @else
                                    <p><input type="checkbox"> {{$value}}</p>
                                @endif
                            @endforeach
                        </td>
                        <td>
                            <p><b>I. POKOK PERSOALAN</b></p>
                            <p>{!! $letter->pokok_persoalan !!}</p>
                            <br/>
                            <br/>
                            <p><b>II. PRA ANGGAPAN</b></p>
                            <p>{!! $letter->pra_anggapan !!}</p>
                            <br/>
                            <br/>

                            <p><b>III. FAKTA DAN DATA YANG BERPENGARUH TERHADAP PERSOALAN</b></p>
                            <p>{!! $letter->fakta_dan_data_yang_berpengaruh !!}</p>
                            <br/>
                            <br/>
                            <p><b>IV. KESIMPULAN</b></p>
                            <p>{!! $letter->fakta_dan_data_yang_berpengaruh !!}</p>
                            <br/>
                            <br/>
                            <p><b>V. SARAN</b></p>
                            <p>{!! $letter->saran !!}</p>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <p style="text-align: right;margin-right: 50px">Sekretaris,</p>
                            <br/>
                            <br/>
                            <br/>
                            <p style="text-align: right;margin-right: 30px; "><b><u>{{$sekretaris->name}}</u></b></p>
                            <p style="text-align: right;margin-right: 40px; ">{{$sekretaris->nip}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
