@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <section class="content-header">
                    <h1>
                        Detail Surat Disposisi
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-building"></i> Surat Disposisi</a></li>
                        <li><a href="#" class="active">Detail Surat Disposisi</a></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Detail Surat Disposisi</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="col-xs-12 table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <td colspan="2" class="text-center"><b>LEMBAR DISPOSISI</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <p>Surat dari: {{$letter->dari }}</p>
                                                    <p>No Surat: {{$letter->letter_no}}</p>
                                                    <p>Tanggal Surat: {{LetterHelper::convertDateToReadableFormat($letter->tanggal_surat)}}</p>
                                                </td>
                                                <td>
                                                    <p>Diterima Tgl: {{ LetterHelper::convertDateToReadableFormat($letter->tanggal_diterima)  }}</p>
                                                    <p>No. Agenda: {{ $letter->no_agenda  }}</p>
                                                    <p>Sifat {{ $letter->sifat  }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4>Perihal</h4>
                                                    <p>{{ $letter->perihal  }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h4>Diteruskan kepada Yth:</h4>
                                                    @foreach($letter->employees as $employee)
                                                        <p>
                                                            Nama  : {{ $employee->name  }}( {{ $employee->position }} )
                                                            Status :
                                                            @if($employee->status_accept===null)
                                                                    <span class="label label-warning">Menunggu Respon</span>
                                                                @elseif($employee->status_accept)
                                                                <span class="label label-success">Diterima</span>
                                                                @else
                                                                <span class="label label-danger">Ditolak</span>
                                                                Alasan: {{ $employee->reason  }}
                                                            @endif
                                                        </p>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <h4>Dengan hormat Harap</h4>
                                                    <?php $i=1;?>
                                                    @foreach($dengan_hormat as $key => $value)
                                                        @if($key==$letter->dengan_hormat_harap)
                                                            <p><input type="checkbox" checked disabled> {{$i}}  {{$value}}</p>
                                                        @else
                                                            <p><input type="checkbox" disabled > {{$i}} {{$value}}</p>
                                                        @endif
                                                        <?php $i++;?>
                                                    @endforeach
                                                    <p>{{ $letter->diteruskan_kepada_custom }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4>Catatan</h4>
                                                    <p>{{ $letter->catatan  }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="{{ URL::asset('storage/'.$letter->attachment)  }}" target="_blank"><img src="{{ URL::asset('storage/'.$letter->attachment)  }}" height="300" ></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>

@endsection

