<div class="box-body">
    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('kepada') ? 'has-error' : '' !!}">
                    {!! Form::label('kepada', 'Kepada Yth') !!}
                    {!! Form::text('kepada', isset($model) ? $model->kepada: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('kepada', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('dari') ? 'has-error' : '' !!}">
                    {!! Form::label('dari', 'Surat dari') !!}
                    {!! Form::text('dari', isset($model) ? $model->dari: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('dari', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('tanggal_surat') ? 'has-error' : '' !!}">
                    {!! Form::label('tanggal_surat', 'Tanggal Surat') !!}
                    {!! Form::text('tanggal_surat', isset($model) ? $model->tanggal_surat: null , ['class'=>'form-control datepicker-no-limit']) !!}
                    {!! $errors->first('tanggal_surat', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('letter_no') ? 'has-error' : '' !!}">
                    {!! Form::label('letter_no', 'Nomor Surat') !!}
                    {!! Form::text('letter_no', isset($model) ? $model->letter_no: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('letter_no', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('perihal') ? 'has-error' : '' !!}">
                    {!! Form::label('perihal', 'Perihal') !!}
                    {!! Form::text('perihal', isset($model) ? $model->perihal: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('perihal', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('persetujuan') ? 'has-error' : '' !!}">
                    {!! Form::label('persetujuan', 'Kolom Disposisi') !!}
                    {!! Form::select('persetujuan',array('1'=>'Disetujui','0'=>'Tidak Setuju'), isset($model) ? $model->persetujuan: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('persetujuan', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {!! $errors->has('pokok_persoalan') ? 'has-error' : '' !!}">
        {!! Form::label('pokok_persoalan', 'Pokok persoalan') !!}
        @if(isset($model))
            <textarea class="form-control" name="pokok_persoalan">{{ $model->pokok_persoalan  }}</textarea>
        @else
            <textarea class="form-control" name="pokok_persoalan"></textarea>
        @endif

        {!! $errors->first('pokok_persoalan', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('pra_anggapan') ? 'has-error' : '' !!}">
        {!! Form::label('pra_anggapan', 'Pra anggapan') !!}
        @if(isset($model))
            <textarea class="form-control" id="ckeditor" name="pra_anggapan">{{ $model->pra_anggapan  }}</textarea>
        @else
            <textarea class="form-control" id="ckeditor" name="pra_anggapan"></textarea>
        @endif

        {!! $errors->first('pra_anggapan', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('fakta_dan_data_yang_berpengaruh') ? 'has-error' : '' !!}">
        {!! Form::label('fakta_dan_data_yang_berpengaruh', 'Fakta dan data yang berpengaruh terhadap persoalan') !!}
        @if(isset($model))
            <textarea class="form-control" name="fakta_dan_data_yang_berpengaruh">{{ $model->fakta_dan_data_yang_berpengaruh  }}</textarea>
        @else
            <textarea class="form-control" name="fakta_dan_data_yang_berpengaruh"></textarea>
        @endif

        {!! $errors->first('fakta_dan_data_yang_berpengaruh', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('kesimpulan') ? 'has-error' : '' !!}">
        {!! Form::label('kesimpulan', 'Kesimpulan') !!}
        @if(isset($model))
            <textarea class="form-control" name="kesimpulan">{{ $model->kesimpulan  }}</textarea>
        @else
            <textarea class="form-control" name="kesimpulan"></textarea>
        @endif

        {!! $errors->first('kesimpulan', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('saran') ? 'has-error' : '' !!}">
        {!! Form::label('saran', 'Saran') !!}
        @if(isset($model))
            <textarea class="form-control" name="saran">{{ $model->saran  }}</textarea>
        @else
            <textarea class="form-control" name="saran"></textarea>
        @endif

        {!! $errors->first('saran', '<p class="help-block">:message</p>') !!}
    </div>


</div>
<!-- /.box-body -->

<div class="box-footer">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
@section('script-form')
    <script>

    </script>

@endsection
