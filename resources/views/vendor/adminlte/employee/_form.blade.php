<div class="box-body">
    <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
        {!! Form::label('name', 'Nama') !!}
        {!! Form::text('name', isset($model) ? $model->name: null , ['class'=>'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group {!! $errors->has('nip') ? 'has-error' : '' !!}">
        {!! Form::label('nip', 'NIP') !!}
        {!! Form::text('nip', isset($model) ? $model->nip: null , ['class'=>'form-control']) !!}
        {!! $errors->first('nip', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('address') ? 'has-error' : '' !!}">
        {!! Form::label('address', 'Alamat') !!}
        {!! Form::text('address', isset($model) ? $model->address: null , ['class'=>'form-control']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('phone_number') ? 'has-error' : '' !!}">
        {!! Form::label('phone_number', 'No Telphone') !!}
        {!! Form::text('phone_number', isset($model) ? $model->phone_number: null , ['class'=>'form-control']) !!}
        {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('position') ? 'has-error' : '' !!}">
        {!! Form::label('position', 'Jabatan') !!}
        {!! Form::text('position', isset($model) ? $model->position: null , ['class'=>'form-control','id'=>'position']) !!}
        {!! Form::text('position_id', null , ['class'=>'hidden','id'=>'position_id']) !!}
        {!! $errors->first('position', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('grade') ? 'has-error' : '' !!}">
        {!! Form::label('grade', 'Golongan') !!}
        {!! Form::text('grade', isset($model) ? $model->grade: null , ['class'=>'form-control','id'=>'grade']) !!}
        {!! Form::text('grade_id', null , ['class'=>'hidden','id'=>'grade_id']) !!}
        {!! $errors->first('grade', '<p class="help-block">:message</p>') !!}
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
@section('script-form')
    <script>
        $('#position').autocomplete({
            source:'{{ route('position.get_list') }}',
            minLength:2,
            autoFocus:true,
            select:function(e,ui)
            {
                $('#position').val(ui.item.value);
                $('#position_id').val(ui.item.id);
            }
        });

        $('#degree').autocomplete({
            source:'{{ route('grade.get_list') }}',
            minLength:2,
            autoFocus:true,
            select:function(e,ui)
            {
                $('#grade').val(ui.item.value);
                $('#grade_id').val(ui.item.id);
            }
        });
    </script>

@endsection
