
<div class="box-body">
  <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
    {!! Form::label('name', 'Nama (Tanpa spasi)') !!}
    {!! Form::text('name', isset($model) ? $model->name: null , ['class'=>'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
  </div>
  <div class="form-group {!! $errors->has('deskripsi') ? 'has-error' : '' !!}">
    {!! Form::label('description', 'Deskripsi') !!}
    {!! Form::text('description', isset($model) ? $model->description: null , ['class'=>'form-control']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
  {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
