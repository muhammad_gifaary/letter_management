@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<section class="content-header">
		      <h1>
		        Data Permission User
		      </h1>
		      <ol class="breadcrumb">
		        <li><a href="#"><i class="fa fa-building"></i> Permission User</a></li>
		        <li><a href="#" class="active">List Permission User</a></li>
		      </ol>
		    </section>

		    <!-- Main content -->
		    <section class="content">
		      <div class="row">
		        <div class="col-xs-12">
		          <div class="box">
		            <div class="box-header">
		              <h3 class="box-title">List Permission</h3>
		              <div class="pull-right">
		              		<a href="{{ route('permission.create') }}" class="btn  btn-success btn-flat btn-sm" title="Tambah" ><span class="glyphicon glyphicon-plus"></span></a>
		              </div>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="permission" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  <th>Nama</th>
			                  <th>Deskripsi</th>
			                  <th>Aksi</th>
			                </tr>
		                </thead>
		              </table>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
		        <!-- /.col -->
		      </div>
		      <!-- /.row -->
		    </section>
		</div>
	</div>
</div>

@endsection
@section('additional-script')
<script>
    $(function () {
        $('#permission').DataTable({
            serverSide: true,
            processing: true,
            ajax: '{{ route('permission.data') }}',
            columns: [
            	{data: 'name'},
                {data: 'description'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection
