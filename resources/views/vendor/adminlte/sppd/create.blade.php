@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
              <h1>
                Data SPPD
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-building"></i> Surat Perintah Perjalanan Dinas</a></li>
                <li><a href="#" class="active">Surat Perintah Perjalanan Dinas</a></li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::open(['route' => ['sppd.store',$letter->id], 'method' => 'post','files'=>true]) !!}
                            @include('adminlte::sppd._form',['letter'=>$letter,'employee'=>$employee])
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </section>
        </div>
    </div>
</div>

@endsection
