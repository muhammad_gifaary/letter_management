<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> SmartMailPU </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('/css/all.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        p,h3,h5,h4 {
            margin: 0px;
            padding: 0px;
        }
        td,body{
            font-size: 12px;
        }
        .logo{
            margin-left: 50px;
        }
    </style>
    <style type="text/css" media="print">
        .page{
            page-break-inside: avoid;
        }
        .logo{
            margin-left: 150px;
        }
    </style>

</head>
<body style="margin:20px">
@foreach($letters->child as $letter)
    <div class="page" style="page-break-after: always">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="col-6 col-md-6">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div style="float: left;" class="logo"><img src="{{asset('img/logo.png')}}" height="45"></div>
                            <div style="float: left">
                                <div class="center" style="text-align: center">
                                    <h4>PEMERINTAH PROVINSI KALIMANTAN UTARA</h4>
                                    <h4>DINAS PEKERJAAN UMUM, PENATAAN RUANG,</h4>
                                    <h4>PERUMAHAN DAN KAWASAN PERMUKIMAN</h4>
                                    <p>Jalan Agathis Telp. (0552) 21490 Fax. (0552) 21542, e-mail : dpu.kaltaraprov@yahoo.co.id </p>
                                    <p>TANJUNG SELOR Kode Pos 77212</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="border: 2px solid #000"/>
                    <div class="row" style="margin-top: 20px; width: 100%">
                        <table style="width: 50%; margin-left: 10%">
                            <tr>
                                <td class="text-right" style="width: 200px">Kode Nomor</td>
                                <td class="text-right">{{$letter->kode_nomor}}</td>
                            </tr>
                            <tr>
                                <td class="text-right">Nomor SPPD</td>
                                <td class="text-right">{{$letter->letter_no}}</td>
                            </tr>
                        </table>
                    </div>

                    <div class="row" style="margin-top: 20px">
                        <div class="text-center">
                            <h5><b>SURAT PERINTAH PERJALANAN DINAS</b></h5>
                            <h5><b>Nomor: {{ $letter->letter_no  }}</b></h5>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 20px">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td style="width: 250px">1. Pejabat Yang Berwenang Memberi Perintah</td>
                                    <td>{{$letter->pejabat_berwenang}}</td>
                                </tr>
                                <tr>
                                    <td>2. Nama / Nip Pegawai Yang Diperintahkan</td>
                                    <td>{{$letter->employeeSppd->name}}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>3. a. Pangkat Gol. Ruang Menurut PP No. 6 Tn 1997</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Jabatan/Intansi</p>
                                    </td>
                                    <td>
                                        <p>a. {{$letter->employeeSppd->grade}}</p>
                                        <p>b. {{$letter->employeeSppd->position}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4. Maksud Perjalanan Dinas</td>
                                    <td>{!! $letters->maksud_perjalanan_dinas !!}</td>
                                </tr>
                                <tr>
                                    <td>5. Alat angkutan yang dipergunakan</td>
                                    <td>{!! $letter->alat_angkutan !!}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>6. a. Tempat berangkat</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Tempat tujuan</p>
                                    </td>
                                    <td>
                                        <p>a. {{$letter->tempat_berangkat}}</p>
                                        <p>b. {{$letter->tempat_tujuan}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>7. a. Lamanya perjalanan dinas</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Tempat berangkat</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c. Tempat harus kembali</p>
                                    </td>
                                    <td>
                                        <p>a. {{ LetterHelper::diffDay($letters->tgl_berangkat,$letters->tgl_kembali) }} hari</p>
                                        <p>b. {{LetterHelper::convertDateToReadableFormat($letters->tgl_berangkat)}}</p>
                                        <p>c. {{LetterHelper::convertDateToReadableFormat($letters->tgl_kembali)}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>8. Pengikut</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Tempat berangkat</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c. Tempat harus kembali</p>
                                    </td>
                                    <td>
                                        <p>{{$letter->nama_pengikut}}</p>
                                        <p>{{$letter->nip_atau_umur_pengikut}}</p>
                                        <p>{{$letter->hubungan_pengikut}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p>9. Pembebanan aggaran</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Instansi</p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Kode rekening</p>
                                    </td>
                                    <td>
                                        <p>{{$letter->pembebanan_anggaran}}</p>
                                        <p>{{$letter->pembebanan_anggaran_intansi}}</p>
                                        <p>{{$letter->pembebanan_anggaran_kode_rekening}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10. Keterangan</td>
                                    <td>{!! $letter->keterangan !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <p><u>Tembusan disampaikan kepada</u></p>
                            <div>{!! $letters->tembusan !!}</div>
                            <br/>
                            <br/><br/><br/><br/>
                            <p><u>Catatan</u></p>
                            {!! $letter->catatan !!}
                        </div>
                        <div class="com-md-6">
                            <div style="float: right">
                                <table class="text-left">
                                    <tr>
                                        <td style="width: 100px">Dikeluarkan di</td>
                                        <td>{{$letters->ditetapkan_di}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pada tanggal</td>
                                        <td>{{LetterHelper::convertDateToReadableFormat($letters->tgl_penetapan)}}</td>
                                    </tr>
                                </table>
                                <div class="text-center">
                                    <br/>
                                    <p><b>An. Kepada Dinas</b></p>
                                    <br/>
                                    <br/>
                                    <p><u><b>{{$sekretaris->name}}</b></u></p>
                                    <p><b>{{$sekretaris->nip}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-6">
                    <div class="row">
                        <div class="col-md-8">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <p>I. Berangkat dari</p>
                                    </td>
                                    <td>{{ $letter->tempat_berangkat  }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Tanggal</td>
                                    <td>{{LetterHelper::convertDateToReadableFormat($letters->tgl_berangkat)}}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Ke</td>
                                    <td>{{$letters->tujuan}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center">
                                <p><b>Pejabat Pelaksana Teknis Kegiatan</b></p>
                                <br/>
                                <br/>
                                <p><u><b>{{$letter->employeeSppd->name}}</b></u></p>
                                <p><b>{{$letter->employeeSppd->nip}}</b></p>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 20px">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <p>II. Tiba di&nbsp;&nbsp;&nbsp;&nbsp; {{$letter->tempat_tujuan}}</p>
                                        <p>&nbsp;&nbsp;&nbsp; Pada tanggal&nbsp;&nbsp;&nbsp;&nbsp; {{LetterHelper::convertDateToReadableFormat($letters->tgl_kembali)}}</p>
                                    </td>
                                    <td>
                                        <p>&nbsp;&nbsp;&nbsp;Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp; {{$letter->tempat_tujuan}}</p>
                                        <p>&nbsp;&nbsp;&nbsp; Ke&nbsp;&nbsp;&nbsp;&nbsp; {{$letter->tempat_berangkat}}</p>
                                        <p>&nbsp;&nbsp;&nbsp; Pada tanggal &nbsp;&nbsp;&nbsp;&nbsp; {{LetterHelper::convertDateToReadableFormat($letters->tgl_kembali)}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p>III. Tiba di&nbsp;&nbsp;&nbsp;&nbsp; :</p>
                                        <p>&nbsp;&nbsp;&nbsp; Pada tanggal&nbsp;&nbsp;&nbsp;&nbsp; :</p>
                                    </td>
                                    <td>
                                        <p>&nbsp;&nbsp;&nbsp;Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp; :</p>
                                        <p>&nbsp;&nbsp;&nbsp; Ke&nbsp;&nbsp;&nbsp;&nbsp; :</p>
                                        <p>&nbsp;&nbsp;&nbsp; Pada tanggal &nbsp;&nbsp;&nbsp;&nbsp; :</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <p>Tiba kembali di {{$letter->tempat_tujuan}}</p>
                                        <p>Pad tanggal {{ LetterHelper::diffDay($letters->tgl_berangkat,$letters->tgl_kembali) }} hari</p>
                                        <p>Telah diperiksa dengan ketentuan bahwa Perjalanan Dinas tersebut diatas benar dilakuan atas perintahnya dan semata-mata untuk kepentingan jabatan dalam waktu yang sesingkat-singkatnya.</p>
                                        <div class="text-center"  style="margin-left: 350px">
                                            <br/>
                                            <p><b>An. Kepada Dinas</b></p>
                                            <br/>
                                            <br/>
                                            <p><u><b>{{$sekretaris->name}}</b></u></p>
                                            <p><b>{{$sekretaris->nip}}</b></p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <p>VII Catatan Lain-lain</p>
                                <p>{!! $letter->catatan_lain_lain !!}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>VII. Perhatian</p>
                                <p>{!! $letter->perhatian !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
<!-- ./wrapper -->
</body>
</html>
