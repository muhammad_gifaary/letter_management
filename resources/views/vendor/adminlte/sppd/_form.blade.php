<div class="box-body">
    @foreach($employee as $model)
        <h3>SPPD</h3>
        <p>Nama {{$model->name}}</p>
        <p>NIP {{$model->nip}}</p>
        <p>Jabatan {{$model->position}}</p>
        <p>Golongan {{$model->grade}}</p>

        <div class="row">
            <div class="col-12">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][letter_no]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][letter_no]", 'Nomor SPPD') !!}
                        {!! Form::text("data[$model->id][letter_no]", isset($model) ? $model->letter_no: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][letter_no]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][kode_nomor]") ? 'has-error' : '' !!}">
                        {!! Form::label('kode_nomor', 'Kode Nomor') !!}
                        {!! Form::text("data[$model->id][kode_nomor]", isset($model) ? $model->kode_nomor: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][kode_nomor]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][pejabat_berwenang]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][pejabat_berwenang]", 'Pejabat Yang Berwenang Memberi Perintah') !!}
                        {!! Form::text("data[$model->id][pejabat_berwenang]", isset($model) ? $model->pejabat_berwenang: 'Kepala Dinas Pekerjaan Umum, Penataan Ruang, Perumahan' , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][pejabat_berwenang]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][alat_agkutan]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][alat_angkutan]", 'Alat angkutan yang dipergunakan') !!}
                        {!! Form::text("data[$model->id][alat_angkutan]", 'Taxi Darat, laut dan Udara' , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][alat_angkutan]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][tempat_berangkat]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][tempat_berangkat]", 'Tempat Berangkat') !!}
                        {!! Form::text("data[$model->id][tempat_berangkat]", isset($model) ? $model->tempat_berangkat: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][tempat_berangkat]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][tempat_tujuan]") ? 'has-error' : '' !!}">
                        {!! Form::label('tempat_tujuan', 'Tempat Tujuan') !!}
                        {!! Form::text("data[$model->id][tempat_tujuan]", isset($model) ? $model->tempat_tujuan: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][tempat_tujuan]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="col-md-4 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][nama_pengikut]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][nama_pengikut]", 'Nama pengikut') !!}
                        {!! Form::text("data[$model->id][nama_pengikut]", isset($model) ? $model->nama_pengikut: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][nama_pengikut]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][nip_atau_umur_pengikut]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][nip_atau_umur_pengikut]", 'NIP/Umur') !!}
                        {!! Form::text("data[$model->id][nip_atau_umur_pengikut]", isset($model) ? $model->nip_atau_umur_pengikut: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][nip_atau_umur_pengikut]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="form-group {!! $errors->has("data[$model->id][hubungan_pengikut]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][hubungan_pengikut]", 'Hubungan Keluarga') !!}
                        {!! Form::text("data[$model->id][hubungan_pengikut]", isset($model) ? $model->hubungan_pengikut: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][hubungan_pengikut]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="col-md-4 col-sm-12">
                    <div class="form-group {!! $errors->has("pembebanan_anggaran[$model->id]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][pembebanan_anggaran]", 'Pembebanan Anggaran') !!}
                        {!! Form::text("data[$model->id][pembebanan_anggaran]", isset($model) ? $model->pembebanan_anggaran: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][pembebanan_anggaran]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="form-group {!! $errors->has("pembebanan_anggaran_instansi[$model->id]") ? 'has-error' : '' !!}">
                        {!! Form::label("data[$model->id][pembebanan_anggaran_instansi]", 'Instansi') !!}
                        {!! Form::text("data[$model->id][pembebanan_anggaran_instansi]", isset($model) ? $model->pembebanan_anggaran_instansi: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][pembebanan_anggaran_instansi]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="form-group {!! $errors->has("pembebanan_anggaran_kode_rekening[$model->id]") ? 'has-error' : '' !!}">
                        {!! Form::label("pembebanan_anggaran_kode_rekening[$model->id]", 'Kode Rekening') !!}
                        {!! Form::text("data[$model->id][pembebanan_anggaran_kode_rekening]", isset($model) ? $model->pembebanan_anggaran_kode_rekening: null , ['class'=>'form-control']) !!}
                        {!! $errors->first("data[$model->id][pembebanan_anggaran_kode_rekening]", '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group {!! $errors->has("keterangan[$model->id]") ? 'has-error' : '' !!}">
            {!! Form::label("keterangan[$model->id]", 'Keterangan') !!}
            @if(isset($model))
                <textarea class="form-control" name="data[{{$model->id}}][keterangan]">{{ $model->keterangan  }}</textarea>
            @else
                <textarea class="form-control" name="data[{{$model->id}}][keterangan]"></textarea>
            @endif

            {!! $errors->first("data[$model->id]['keterangan']", '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {!! $errors->has("data[$model->id][catatan]") ? 'has-error' : '' !!}">
            {!! Form::label("catatan[$model->id]", 'Catatan') !!}
            @if(isset($model))
                <textarea class="form-control" name="data[{{$model->id}}][catatan]">{{ $model->catatan  }}</textarea>
            @else
                <textarea class="form-control" name="data[{{$model->id}}][catatan]"></textarea>
            @endif

            {!! $errors->first("catatan[$model->id]", '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {!! $errors->has("catatan_lain_lain[$model->id]") ? 'has-error' : '' !!}">
            {!! Form::label("catatan_lain_lain[$model->id]", 'Catatan Lain-Lain') !!}
            @if(isset($model))
                <textarea class="form-control" name="data[{{$model->id}}][catatan_lain_lain]">{{ $model->catatan_lain_lain  }}</textarea>
            @else
                <textarea class="form-control" name="data[{{$model->id}}][catatan_lain_lain]"></textarea>
            @endif

            {!! $errors->first("catatan_lain_lain[$model->id]", '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {!! $errors->has("perhatian[$model->id]") ? 'has-error' : '' !!}">
            {!! Form::label("perhatian[$model->id]", 'Perhatian') !!}
            @if(isset($model))
                <textarea class="form-control" name="data[{{$model->id}}][perhatian]">{{ $model->perhatian  }}</textarea>
            @else
                <textarea class="form-control" name="data[{{$model->id}}][perhatian]"></textarea>
            @endif

            {!! $errors->first("perhatian[$model->id]", '<p class="help-block">:message</p>') !!}
        </div>
    @endforeach

</div>
<!-- /.box-body -->

<div class="box-footer">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
@section('script-form')
    <script>

    </script>

@endsection
