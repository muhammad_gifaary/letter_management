@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <section class="content-header">
                    <h1>
                        Respon Surat Disposisi
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-building"></i> Surat Disposisi</a></li>
                        <li><a href="#" class="active">Respon Surat Disposisi</a></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Respon Surat Disposisi</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="col-xs-12 table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <td colspan="2" class="text-center"><b>LEMBAR DISPOSISI</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <p>Surat dari: {{$letter->dari }}</p>
                                                    <p>No Surat: {{$letter->letter_no}}</p>
                                                    <p>Tanggal Surat: {{LetterHelper::convertDateToReadableFormat($letter->tanggal_surat)}}</p>
                                                </td>
                                                <td>
                                                    <p>Diterima Tgl: {{ LetterHelper::convertDateToReadableFormat($letter->tanggal_diterima)  }}</p>
                                                    <p>No. Agenda: {{ $letter->no_agenda  }}</p>
                                                    <p>Sifat {{ $letter->sifat  }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4>Perihal</h4>
                                                    <p>{{ $letter->perihal  }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h4>Dengan hormat Harap</h4>
                                                   {{ $letter->getDenganHormat($letter->dengan_hormat_harap)  }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h4>Catatan</h4>
                                                    <p>{{ $letter->catatan  }}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center">
                                                    @if($letterEmployee->status_accept!=null)
                                                        {!! Form::open(['route' => ['disposition.response.store',$letter->id], 'method' => 'post']) !!}
                                                        <div class="box-body">
                                                            <div class="form-group {!! $errors->has('status_accept') ? 'has-error' : '' !!}">
                                                                {!! Form::label('status_accept', 'Respon') !!}
                                                                {!! Form::select('status_accept',array('1'=>'Setujui','0'=>'Tidak disetujui'), null , ['class'=>'form-control']) !!}
                                                                {!! $errors->first('status_accept', '<p class="help-block">:message</p>') !!}
                                                            </div>

                                                            <div class="form-group {!! $errors->has('reason') ? 'has-error' : '' !!}">
                                                                {!! Form::label('reason', 'Alasan') !!}
                                                                <textarea class="form-control" name="reason"></textarea>
                                                                {!! $errors->first('reason', '<p class="help-block">:message</p>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="box-footer">
                                                            {!! Form::submit('Respon', ['class'=>'btn btn-primary btn-block']) !!}
                                                        </div>
                                                        {!! Form::close() !!}
                                                    @else
                                                        <h3><span class="label label-success">Terimakasih telah merespon</span></h3>
                                                    @endif
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>

@endsection

