@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <section class="content-header">
                    <h1>
                        Data Setting
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-building"></i> Pengaturan</a></li>
                        <li><a href="#" class="active">Daftar Pengaturan</a></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Daftar Pengaturan</h3>
                                    <div class="pull-right">
                                        <a href="{{ route('role.create') }}" class="btn  btn-success btn-flat btn-sm" title="Tambah" ><span class="glyphicon glyphicon-plus"></span></a>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="role" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Pengaturan</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($settings as $setting)
                                                <tr>
                                                    <td>{{ $setting->name }}</td>
                                                    <td>
                                                        @if($setting->id<=8)
                                                            <input value="{{$setting->value1}}" id="{{$setting->code}}" class="autocomplete_employee form-control">
                                                            <input value="{{  $setting->value2  }}" id="employee_id_{{$setting->code}}" type="hidden" >
                                                            @else
                                                            <input value="{{$setting->value1}}" id="{{$setting->code}}" class="form-control">
                                                            <input value="{{  $setting->value2  }}" id="employee_id_{{$setting->code}}" type="hidden" >
                                                            @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>

@endsection
@section('additional-script')
    <script>
        $(function () {
            $('#role').DataTable({
                displayLength : 50
            });
            $('.autocomplete_employee').autocomplete({
                source:'{{ route('employee.get_list') }}',
                minLength:2,
                autoFocus:true,
                select:function(e,ui)
                {
                    $(this).val(ui.item.value);
                    $('#employee_id_'+$(this).attr('id')).val(ui.item.id);

                    $.ajax({
                        url: "{{route('setting.store')}}",
                        type: 'POST',
                        data : {
                            _token : '{!! csrf_token() !!}',
                            code : $(this).attr('id'),
                            value1 : $(this).val(),
                            value2 : $("#employee_id_"+$(this).attr('id')).val()
                        },
                        success: function(result) {
                            console.log(result);
                        }
                    });
                }
            });


        });
    </script>
@endsection
