<div class="box-body">
    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('letter_no') ? 'has-error' : '' !!}">
                    {!! Form::label('letter_no', 'Nomor Surat') !!}
                    {!! Form::text('letter_no', isset($model) ? $model->letter_no: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('letter_no', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('tujuan') ? 'has-error' : '' !!}">
                    {!! Form::label('tujuan', 'Tujuan') !!}
                    {!! Form::text('tujuan', isset($model) ? $model->tujuan: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('tujuan', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {!! $errors->has('employees[0]') ? 'has-error' : '' !!}">
        {!! Form::label('employee', 'Kepada') !!}
        {!! Form::select('employees[]',$employee, isset($model) ? $model->employees->pluck('employee_id'): null , ['class'=>'form-control autocomplete_employee','multiple'=>'true']) !!}
        {!! $errors->first('employees[0]', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('tgl_berangkat') ? 'has-error' : '' !!}">
                    {!! Form::label('tgl_berangkat', 'Tanggal Berangkat') !!}
                    {!! Form::text('tgl_berangkat', isset($model) ? $model->tgl_berangkat: null , ['class'=>'form-control datepicker-no-limit']) !!}
                    {!! $errors->first('tgl_berangkat', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('tgl_kembali') ? 'has-error' : '' !!}">
                    {!! Form::label('tgl_kembali', 'Tanggal Kembali') !!}
                    {!! Form::text('tgl_kembali', isset($model) ? $model->tgl_kembali: null , ['class'=>'form-control datepicker-no-limit']) !!}
                    {!! $errors->first('tgl_kembali', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
       <div class="col-12">
           <div class="col-md-6 col-sm-12">
               <div class="form-group {!! $errors->has('ditetapkan_di') ? 'has-error' : '' !!}">
                   {!! Form::label('ditetapkan_di', 'Ditetapkan di') !!}
                   {!! Form::text('ditetapkan_di', isset($model) ? $model->ditetapkan_di: "Tanjung Selor" , ['class'=>'form-control']) !!}
                   {!! $errors->first('ditetapkan_di', '<p class="help-block">:message</p>') !!}
               </div>
           </div>
           <div class="col-md-6 col-sm-12">
               <div class="form-group {!! $errors->has('tgl_penetapan') ? 'has-error' : '' !!}">
                   {!! Form::label('tgl_penetapan', 'Tanggal Penetapan') !!}
                   {!! Form::text('tgl_penetapan', isset($model) ? $model->tgl_penetapan: null , ['class'=>'form-control datepicker-no-limit']) !!}
                   {!! $errors->first('tgl_penetapan', '<p class="help-block">:message</p>') !!}
               </div>
           </div>
       </div>
    </div>

    <div class="form-group {!! $errors->has('maksud_perjalanan_dinas') ? 'has-error' : '' !!}">
        {!! Form::label('maksud_perjalanan_dinas', 'Maksud Perjalanan Dinas') !!}
        @if(isset($model))
            <textarea class="form-control" name="maksud_perjalanan_dinas">{{ $model->maksud_perjalanan_dinas  }}</textarea>
        @else
            <textarea class="form-control" name="maksud_perjalanan_dinas"></textarea>
        @endif

        {!! $errors->first('maksud_perjalanan_dinas', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('tembusan') ? 'has-error' : '' !!}">
        {!! Form::label('tembusan', 'Tembusan') !!}
        @if(isset($model))
            <textarea class="form-control" name="tembusan">{{ $model->tembusan  }}</textarea>
        @else
            <textarea class="form-control" name="tembusan"></textarea>
        @endif

        {!! $errors->first('tembusan', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
    {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
@section('script-form')
    <script>
        $('.autocomplete_employee').select2({
            ajax: {
                    url: "{{ route('employee.get_list') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            term: params.term, // search term
                            _token : '{!! csrf_token() !!}'
                        };
                    },
                processResults: function (data) {
                    // Tranforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data
                    };
                }
            }
        });


    </script>

@endsection
