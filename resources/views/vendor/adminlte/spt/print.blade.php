<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> SmartMailPU </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('/css/all.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        p,h3,h5,h4 {
            margin: 0px;
            padding: 0px;
        }
        .logo{
            margin-left: 350px;
        }
    </style>
    <style type="text/css" media="print">
        .logo{
            margin-left: 50px;
        }
    </style>
</head>
<body style="margin:20px">
<div class="page">
    <div class="row">
        <div class="col-md-12 col-12">
            <div style="float: left;" class="logo"><img src="{{asset('img/logo.png')}}" height="45"></div>
            <div style="float: left">
                <div class="center" style="text-align: center">
                    <h4>PEMERINTAH PROVINSI KALIMANTAN UTARA</h4>
                    <h4>DINAS PEKERJAAN UMUM, PENATAAN RUANG,</h4>
                    <h4>PERUMAHAN DAN KAWASAN PERMUKIMAN</h4>
                    <p>Jalan Agathis Telp. (0552) 21490 Fax. (0552) 21542, e-mail : dpu.kaltaraprov@yahoo.co.id </p>
                    <p>TANJUNG SELOR Kode Pos 77212</p>
                </div>
            </div>
        </div>
    </div>
    <hr style="border: 2px solid #000"/>

    <div class="row" style="margin-top: 20px">
        <div class="text-center">
            <h5><b>SURAT PERINTAH TUGAS</b></h5>
            <h5><b>Nomor: {{ $letter->letter_no  }}</b></h5>
        </div>
    </div>

    <div class="row" style="margin-top: 20px">
        <div class="col-xs-12 table-responsive">
            <table class="table">
                <tr>
                    <td>Dasar</td>
                    <td>1</td>
                    <td>
                        <p>Peraturan Gubernur kalimantan Utara Nomor 12 Tahun 2016 Tentang Perubahan Atas Peraturan Gubernur Nomor 47 Tahun 2015 Tentang Pedoman Pelaksanaan Perjalanan Dinas Bagi Gubernur /Wakil Gubernur, Pimpinan Dan Anggota DPRD, Pegawai Negeri Sipil Daerah, Calon Pegawai  Negeri Sipil Daerah, Dan Tenaga Non Pegawai Negeri Sipil di Lingkungan Pemerintahan Daerah Provinsi Kalimantan Utara</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>2</td>
                    <td>Berdasarkan Keputusan Gubernur Kalimantan Utara Nomor 188.44/K.696 Tahun 2017 Tentang Biaya Perjalanan Dinas Bagi Gubernur/Wakil Gubernur, Pimpinan Dan Anggota DPRD, Pegawai Negeri Sipil, Calon Pegawai Negeri Sipil, Dan Tenaga Non Pegawai Negeri Sipil Di Lingkungan Pemerintahan Daerah Provinsi Kalimantan Utara</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row" style="margin-top: 10px">
        <div class="text-center">
            <h5><b>MEMERINTAHKAN:</b></h5>
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 10px">
        <div class="col-12">
            <p>Kepada</p>
        </div>
        <div class="col-xs-12 table-responsive">
            @foreach($letter->employees  as $employee)
                <div style="margin-bottom: 10px">
                    <table>
                        <tr>
                            <td style="width: 200px">Nama</td>
                            <td style="width: 10px">:</td>
                            <td>{{$employee->name}}</td>
                        </tr>
                        <tr>
                            <td>Pangkat/Golongan</td>
                            <td style="width: 10px">:</td>
                            <td>{{$employee->grade}}</td>
                        </tr>
                        <tr>
                            <td>NIP</td>
                            <td style="width: 10px">:</td>
                            <td>{{$employee->nip}}</td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td style="width: 10px">:</td>
                            <td>{{$employee->position}}</td>
                        </tr>
                    </table>
                </div>
            @endforeach
        </div>
    </div>

    <div class="row" style="margin-top: 10px;margin-left: 10px">
        <div class="col-12">
            <table>
                <tr>
                    <td style="width: 200px">Maksud Perjalanan Dinas</td>
                    <td>{!! $letter->maksud_perjalanan_dinas !!}</td>
                </tr>
            </table>
        </div>
        <div class="col-12" style="margin-top: 10px">
            <table>
                <tr>
                    <td style="width: 200px">Tujuan</td>
                    <td style="width: 10px">:</td>
                    <td>{{$letter->tujuan}}</td>
                </tr>
                <tr>
                    <td>Lamanya</td>
                    <td style="width: 10px">:</td>
                    <td>{{ LetterHelper::diffDay($letter->tgl_berangkat,$letter->tgl_kembali) }} hari</td>
                </tr>
                <tr>
                    <td>Tanggal Berangkat</td>
                    <td style="width: 10px">:</td>
                    <td>{{LetterHelper::convertDateToReadableFormat($letter->tgl_berangkat)}}</td>
                </tr>
                <tr>
                    <td>Tanggal Kembali</td>
                    <td style="width: 10px">:</td>
                    <td>{{LetterHelper::convertDateToReadableFormat($letter->tgl_kembali)}}</td>
                </tr>
            </table>
        </div>
        <div class="col-12" style="margin-top: 15px; margin-left: 10px">
            <p>Setelah melaksanakan tugas, agar segera membuat laporan</p>
            <p>Demikian surat perintah tugas ini diberikan agar dipergunakan sebagimana mestinya.</p>
        </div>

        <div class="col-12" style="margin-top: 15px; margin-right: 30px">
            <div style="float: right">
                <table class="text-left">
                    <tr>
                        <td style="width: 100px">Ditetapkan di</td>
                        <td>{{$letter->ditetapkan_di}}</td>
                    </tr>
                    <tr>
                        <td>Pada tanggal</td>
                        <td>{{LetterHelper::convertDateToReadableFormat($letter->tgl_penetapan)}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12" style="margin-top: 15px; margin-right: 50px; float: right; font-weight: bold">
            <div class="text-center">
                <p><b>An. Kepada Dinas</b></p>
                <p><b>Sekretaris,</b></p>
                <br/>
                <br/>
                <p><u><b>{{$sekretaris->name}}</b></u></p>
                <p><b>{{$sekretaris->grade}}</b></p>
                <p><b>{{$sekretaris->nip}}</b></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12" style="margin-top: 25px; margin-left: 30px">
            <p>Tembusan:</p>
            <p>{!! $letter->tembusan !!}</p>
        </div>
    </div>
</div>
<!-- ./wrapper -->
</body>
</html>
