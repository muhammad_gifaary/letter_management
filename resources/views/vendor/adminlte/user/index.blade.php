@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<section class="content-header">
		      <h1>
		        Data User
		      </h1>
		      <ol class="breadcrumb">
		        <li><a href="#"><i class="fa fa-building"></i> User</a></li>
		        <li><a href="#" class="active">List User</a></li>
		      </ol>
		    </section>

		    <!-- Main content -->
		    <section class="content">
		      <div class="row">
		        <div class="col-xs-12">
		          <div class="box">
		            <div class="box-header">
		              <h3 class="box-title">List User</h3>
		              <div class="pull-right">
		              		<a href="{{ route('user.create') }}" class="btn  btn-success btn-flat btn-sm" title="Tambah" ><span class="glyphicon glyphicon-plus"></span></a>
		              </div>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            <div class="table-responsive">
		              <table id="user" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                	<th>NIP</th>
			                  	<th>Nama</th>
			                  	<th>Role</th>
			                  	<th>Email</th>
			                  	<th>Aksi</th>
			                </tr>
		                </thead>
		              </table>
		            </div>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
		        <!-- /.col -->
		      </div>
		      <!-- /.row -->
		    </section>
		</div>
	</div>
</div>

@endsection
@section('additional-script')
<script>
    $(function () {
        $('#user').DataTable({
            serverSide: true,
            processing: true,
            searchDelay: 1000,
            ajax: '{{ route('user.data') }}',
            columns: [
            	{data: 'nip'},
                {data: 'name'},
                {data: 'roles'},
                {data: 'email'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection
