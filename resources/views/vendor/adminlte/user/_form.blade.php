
<div class="box-body">
  @if(!isset($model))
  <div class="form-group {!! $errors->has('p_karyawan_id') ? 'has-error' : '' !!}">
    {!! Form::label('employee_id', 'Karyawan') !!}
    {!! Form::select('employee_id',$karyawan, isset($model) ? $model->employee->id: null , ['class'=>'form-control select2']) !!}
    {!! $errors->first('employee_id', '<p class="help-block">:message</p>') !!}
  </div>
  @endif
  <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
    {!! Form::label('email', 'Email') !!}
    {!! Form::email('email',isset($model) ? $model->email: null , ['class'=>'form-control']) !!}
    {!! Form::email('email_old',isset($model) ? $model->email: null , ['class'=>'form-control hidden']) !!}
    {!! $errors->first('email_old', '<p class="help-block">:message</p>') !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
  </div>
  <div class="form-group {!! $errors->has('role_id') ? 'has-error' : '' !!}">
    {!! Form::label('role_id', 'Role') !!}
    {!! Form::select('role_id',$role, isset($model) ? $model->roles()->first()->name: null , ['class'=>'form-control select2']) !!}
    {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
  </div>

  <div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password', ['class'=>'form-control']) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<!-- /.box-body -->

<div class="box-footer">
  {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
