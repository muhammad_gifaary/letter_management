<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>
<script src="{{ url ('/js/vendor.js') }}" type="text/javascript"></script>
<script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>

<script>
    var dateToday = new Date();
    $('.sidebar-toggle').click(function(){
        $(".user-panel").toggle();
    });
    $(".select2").select2();
    $('.datepicker').datetimepicker({
        //autoclose: true,
        format : "yyyy-mm-dd",
        startDate: "-7d"
    });
    $('.datepicker-no-limit').datetimepicker({
        //autoclose: true,
        format : "YYYY-MM-DD",
        viewMode:'months'
    });
    $('.datetimepicker-no-limit').datetimepicker({
        //autoclose: true,
        format : "yyyy-mm-dd hh:ii",
        pickTime: true,
        autoclose: true

    });
    $('.datetimepicker').datetimepicker({
        //autoclose: true,
        format : "yyyy-mm-dd hh:ii",
        pickTime: true,
        autoclose: true,
        startDate: "-7d"

    });
    $('.datepicker-multiple').datepicker({
        //autoclose: true,
        format : "yyyy-mm-dd",
        multidate: true,
        startDate: "-7d"
    });
    $('.datepicker-multiple-no-limit').datepicker({
        //autoclose: true,
        format : "yyyy-mm-dd",
        multidate: true
    });

    $(".delete").on('click', function() {
        return confirm('Are You Sure ?')
    });

    $('.numeric_only').bind('keyup paste', function(){this.value = this.value.replace(/[^0-9]/g, ''); });

    $(".curency").on("keyup",function(){
        var rgx = /^[0-9]*\.?[0-9]*$/;
        if($(this).val().match(rgx)){
            return true;
        }else{
            alert("Hanya bisa di input angka dan titik");
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });


    function onlyNumbersWithDot(e) {
        var charCode;
        if (e.keyCode > 0) {
            charCode = e.which || e.keyCode;
        }
        else if (typeof (e.charCode) != "undefined") {
            charCode = e.which || e.keyCode;
        }
        if (charCode == 46)
            return true
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    $("textarea").each(function(){
        CKEDITOR.replace(this, {
            removePlugins: ['image','link']
        } );
    });
</script>
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background-color: #3c8dbc;
    }
</style>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
