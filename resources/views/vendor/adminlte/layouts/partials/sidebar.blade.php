<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Surat</li>
            @can('disposition')
            <li @if(Request::segment(1)=='disposition') class="active" @endif><a href="{{ url('disposition') }}"><i class='fa fa-file-archive-o'></i> <span>Disposisi</span></a></li>
            @endcan
            @can('telaah_staff')
            <li @if(Request::segment(1)=='telaah-staf') class="active" @endif><a href="{{ url('telaah-staf') }}"><i class='fa fa-file-archive-o'></i> <span>Telaah Staf</span></a></li>
            @endcan
            @can('spt_sppd')
            <li @if(Request::segment(1)=='spt') class="active" @endif><a href="{{ url('spt') }}"><i class='fa fa-file-archive-o'></i> <span>SPT & SPPD</span></a></li>
            @endcan
            <li class="header">Master</li>
            <!-- Optionally, you can add icons to the links -->

            @can('employee')
                <li @if(Request::segment(1)=='employee') class="active" @endif><a href="{{ url('employee') }}"><i class='fa fa-child'></i> <span>Pegawai</span></a></li>
            @endcan

            @can('user')
            <li @if(Request::segment(1)=='permission') class="active" @endif><a href="{{ url('permission') }}"><i class='fa fa-minus-circle'></i> <span>Hak Akses</span></a></li>
            <li @if(Request::segment(1)=='setting') class="active" @endif><a href="{{ url('setting') }}"><i class='fa fa-gear'></i> <span>Pengaturan</span></a></li>
            <li @if(Request::segment(1)=='role') class="active" @endif><a href="{{ url('role') }}"><i class='fa fa-users'></i> <span>User Group</span></a></li>
            <li @if(Request::segment(1)=='user') class="active" @endif><a href="{{ url('user') }}"><i class='fa fa-user'></i> <span>User</span></a></li>
            @endcan
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
