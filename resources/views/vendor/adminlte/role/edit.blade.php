@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
              <h1>
                Data Role User
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-building"></i> Role User</a></li>
                <li><a href="#" class="active">Edit Role User</a></li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Edit Role User</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::open(["route" => ["role.update",$role->id], "method" => "put"]) !!}
                            @include('adminlte::role._form', ['model' => $role])
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </section>
        </div>
    </div>
</div>
  
@endsection