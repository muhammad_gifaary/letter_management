@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<section class="content-header">
		      <h1>
		        Data Role User
		      </h1>
		      <ol class="breadcrumb">
		        <li><a href="#"><i class="fa fa-building"></i> Role User</a></li>
		        <li><a href="#" class="active">List Role User</a></li>
		      </ol>
		    </section>

		    <!-- Main content -->
		    <section class="content">
		      <div class="row">
		        <div class="col-xs-12">
		          <div class="box">
		            <div class="box-header">
		              <h3 class="box-title">List Role</h3>
		              <div class="pull-right">
		              		<a href="{{ route('role.create') }}" class="btn  btn-success btn-flat btn-sm" title="Tambah" ><span class="glyphicon glyphicon-plus"></span></a>
		              </div>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            <div class="table-responsive">
		              <table id="role" class="table table-bordered table-striped">
		                <thead>
			                <tr>
								<th>AKSES</th>
								@foreach($roles as $role)
									<th>{{ $role->name }}</th>
								@endforeach
			                </tr>
						</thead>
						<tbody>
						@foreach($permissions as $permission)
							<tr>
								<td>{{ $permission->description }}</td>
								@foreach($roles as $role)
									<td>
										{!! Form::select('permission',$akses, ($role->hasPermissions($permission->name)) ? 1 : 0 , ['class'=>'form-control permission','data-role'=>$role->id,'data-permission'=>$permission->name]) !!}
									</td>
								@endforeach
							</tr>
						@endforeach
						</tbody>
		              </table>
		            </div>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
		        <!-- /.col -->
		      </div>
		      <!-- /.row -->
		    </section>
		</div>
	</div>
</div>

@endsection
@section('additional-script')
<script>
    $(function () {
        $('#role').DataTable({
           	displayLength : 50
        });
		$(".permission").change(function(){
				//get id role and get permission name
				var active = $(this).val();
				var role_id = $(this).data('role');
				var permission_name = $(this).data('permission');
				var link = "{{ route('role.update') }}";

				$.ajax({
					url: link+"/"+role_id,
					type: 'PUT',
					data : {
							_token : '{!! csrf_token() !!}',
							active : active,
							permission_name : permission_name,
					},
					success: function(result) {
							console.log(result);
					}
				});
        });
    });
</script>
@endsection
