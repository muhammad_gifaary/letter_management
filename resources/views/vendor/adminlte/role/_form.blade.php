
<div class="box-body">
  <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
    {!! Form::label('name', 'Nama') !!}
    {!! Form::text('name', isset($model) ? $model->name: null , ['class'=>'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
  </div>
  <!--<div class="form-group">
    <input type="checkbox" class="minimal" id="checkAll" value="Check All"> Check All
  </div>
  @foreach($permission as $p)
	  <div class="col-md-4">
      <div class="form-group">
        <input type="checkbox" class="minimal" name="permission[]" value="{{$p->name}}" @if(in_array($p->name,$data['permission'])) {{ "checked" }} @endif> {{$p->deskripsi}}
      </div>
    </div>
  @endforeach-->
</div>
<!-- /.box-body -->

<div class="box-footer">
  {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
@section('additional-script')
<script type="text/javascript">
  $("#checkAll").click(function(){
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
</script>
@endsection
