<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> SmartMailPU </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('/css/all.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        p,h3,h4 {
            margin: 3px 0;
            padding: 0px;
        }
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <div class="row">
            <div class="text-center" style="text-align: center">
                <h3>PEMERINTAH PROVINSI KALIMANTAN UTARA</h3>
                <h3>DINAS PEKERJAAN UMUM, PENATAAN RUANG,</h3>
                <H3>PERUMAHAN DAN KAWASAN PERMUKIMAN</H3>
                <p>Jalan Agathis Telp. (0552) 21490 Fax. (0552) 21542, e-mail : dpu.kaltaraprov@yahoo.co.id </p>
                <p>TANJUNG SELOR Kode Pos 77212</p>
            </div>
        </div>

        <div class="row" style="margin-top: 20px">
            <div class="col-xs-12 table-responsive">
                <table class="table table-bordered" style="width: 100%;">
                    <thead>
                    <tr>
                        <td style="width: 100%; text-align: center"  colspan="2" class="text-center"><b>LEMBAR DISPOSISI</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 50%; margin-bottom: 10px">
                            <p>Surat dari: {{$letter->dari }}</p>
                            <p>No Surat: {{$letter->letter_no}}</p>
                            <p>Tanggal Surat: {{LetterHelper::convertDateToReadableFormat($letter->tanggal_surat)}}</p>
                        </td>
                        <td style="width: 50%; margin-bottom: 10px">
                            <p>Diterima Tgl: {{ LetterHelper::convertDateToReadableFormat($letter->tanggal_diterima)  }}</p>
                            <p>No. Agenda: {{ $letter->no_agenda  }}</p>
                            <p>Sifat {{ $letter->sifat  }}</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h4>Perihal</h4>
                            <p>{{ $letter->perihal  }}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h4 style="margin-top: 10px">Diteruskan kepada Yth</h4>
                            <?php $i=1;?>
                            @foreach($terusan as $key => $value)
                                @if(!empty($letter->diterusakan_kepada) && in_array($key,$letter->diterusakan_kepada))
                                    <p><input type="checkbox" checked style="margin-top: 3px" > {{$i}}  {{$value}}</p>
                                @else
                                    <p><input type="checkbox" style="margin-top: 3px"  > {{$i}} {{$value}}</p>
                                @endif
                                <?php $i++;?>
                            @endforeach

                        </td>
                        <td>
                            <h4 style="margin-top: -10px">Dengan hormat Harap</h4>
                            <?php $i=1;?>
                            @foreach($dengan_hormat as $key => $value)
                                @if($key==$letter->dengan_hormat_harap)
                                    <p><input type="checkbox" style="margin-top: 3px"  checked > {{$i}}  {{$value}}</p>
                                @else
                                    <p><input type="checkbox" style="margin-top: 3px"  > {{$i}} {{$value}}</p>
                                @endif
                                <?php $i++;?>
                            @endforeach
                            <p>{{ $letter->diteruskan_kepada_custom }}</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h4>Catatan</h4>
                            <p>{{ $letter->catatan  }}</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
