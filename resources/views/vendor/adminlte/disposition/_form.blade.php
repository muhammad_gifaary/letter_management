<div class="box-body">
    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('dari') ? 'has-error' : '' !!}">
                    {!! Form::label('dari', 'Surat dari') !!}
                    {!! Form::text('dari', isset($model) ? $model->dari: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('dari', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('letter_no') ? 'has-error' : '' !!}">
                    {!! Form::label('letter_no', 'Nomor Surat') !!}
                    {!! Form::text('letter_no', isset($model) ? $model->letter_no: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('letter_no', '<p class="help-block">:message</p>') !!}
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('tanggal_surat') ? 'has-error' : '' !!}">
                    {!! Form::label('tanggal_surat', 'Tanggal Surat') !!}
                    {!! Form::text('tanggal_surat', isset($model) ? $model->tanggal_surat: null , ['class'=>'form-control datepicker-no-limit']) !!}
                    {!! $errors->first('tanggal_surat', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('tanggal_diterima') ? 'has-error' : '' !!}">
                    {!! Form::label('tanggal_diterima', 'Diterima Tgl') !!}
                    {!! Form::text('tanggal_diterima', isset($model) ? $model->tanggal_diterima: null , ['class'=>'form-control datepicker-no-limit']) !!}
                    {!! $errors->first('tanggal_diterima', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('no_agenda') ? 'has-error' : '' !!}">
                    {!! Form::label('no_agenda', 'Nomor Agenda') !!}
                    {!! Form::text('no_agenda', isset($model) ? $model->no_agenda: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('no_agenda', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('sifat') ? 'has-error' : '' !!}">
                    {!! Form::label('sifat', 'Sifat') !!}
                    {!! Form::select('sifat',$sifat_surat, isset($model) ? $model->sifat: null , ['class'=>'form-control']) !!}
                    {!! $errors->first('sifat', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {!! $errors->has('perihal') ? 'has-error' : '' !!}">
        {!! Form::label('perihal', 'Perihal') !!}
        @if(isset($model))
            <textarea class="form-control" name="perihal">{{ $model->perihal  }}</textarea>
        @else
            <textarea class="form-control" name="perihal"></textarea>
        @endif

        {!! $errors->first('perihal', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="row">
        <div class="col-12">
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('diterusakan_kepada.0') ? 'has-error' : '' !!}">
                    {!! Form::label('diterusakan_kepada', 'Diteruskan Kepada YTH ') !!}
                    {!! Form::select('diterusakan_kepada[]',$terusan, isset($model) ? $model->diterusakan_kepada: null , ['class'=>'form-control select2','multiple'=>'multiple']) !!}
                    {!! $errors->first('diterusakan_kepada.0', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group {!! $errors->has('dengan_hormat_harap') ? 'has-error' : '' !!}">
                    {!! Form::label('dengan_hormat_harap', 'Dengan hormat harap ') !!}
                    {!! Form::select('dengan_hormat_harap',$dengan_hormat, isset($model) ? $model->dengan_hormat_harap: null , ['class'=>'form-control','id'=>'dengan_hormat_harap']) !!}
                    {!! $errors->first('dengan_hormat_harap', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {!! $errors->has('diteruskan_kepada_custom') ? 'has-error' : '' !!}" id="input_diteruskan_kepada_custom">
        {!! Form::label('diteruskan_kepada_custom', 'Diteruskan Kepada') !!}
        {!! Form::text('diteruskan_kepada_custom', isset($model) ? $model->diteruskan_kepada_custom: null , ['class'=>'form-control','id'=>'diteruskan_kepada_custom']) !!}
        {!! $errors->first('diteruskan_kepada_custom', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {!! $errors->has('attachment') ? 'has-error' : '' !!}">
        {!! Form::label('attachment', 'Lampirkan Surat') !!}
        {!! Form::file('attachment',['class'=>'',"accept"=>"image/*"]) !!}
        {!! $errors->first('attachment', '<p class="help-block">:message</p>') !!}
    </div>
    @if(isset($model))
        <a href="{{ URL::asset('storage/'.$model->attachment)  }}" target="_blank"><img src="{{ URL::asset('storage/'.$model->attachment)  }}" height="300" ></a>
    @endif

    <div class="form-group {!! $errors->has('catatan') ? 'has-error' : '' !!}">
        {!! Form::label('catatan', 'Catatan') !!}
        @if(isset($model))
            <textarea class="form-control" name="catatan">{{ $model->catatan }}</textarea>
        @else
            <textarea class="form-control" name="catatan"></textarea>
        @endif

        {!! $errors->first('catatan', '<p class="help-block">:message</p>') !!}
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    {!! Form::submit(isset($model) ? 'Disposisi' : 'Save', ['class'=>'btn btn-primary btn-block']) !!}
</div>
@section('script-form')
    <script>
        $('#input_diteruskan_kepada_custom').hide();
        $('#dengan_hormat_harap').on('change',function () {
            if($(this).val()==='6'){
                $('#input_diteruskan_kepada_custom').show();
            }else{
                $('#diteruskan_kepada_custom').val('');
                $('#input_diteruskan_kepada_custom').hide();
            }
        })
    </script>

@endsection
