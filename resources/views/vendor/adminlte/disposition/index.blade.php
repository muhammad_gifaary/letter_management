@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <section class="content-header">
                    <h1>
                       Data Surat Disposisi
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-building"></i> Disposisi</a></li>
                        <li><a href="#" class="active">Daftar Disposisi</a></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">List Disposisi</h3>
                                    <div class="pull-right">
                                        <a href="{{ route('disposition.create') }}" class="btn  btn-success btn-flat btn-sm" title="Tambah" ><span class="glyphicon glyphicon-plus"></span></a>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="permission" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>No. Surat</th>
                                                <th>No. Agenda</th>
                                                <th>Kepada</th>
                                                <th>Tanggal Diterima</th>
                                                <th>Tanggal Surat</th>
                                                <th>Sifat</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>

@endsection
@section('additional-script')
    <script>
        $(function () {
            $('#permission').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route('disposition.data') }}',
                columns: [
                    {data: 'letter_no'},
                    {data: 'no_agenda'},
                    {data: 'dari'},
                    {data: 'tanggal_diterima'},
                    {data: 'tanggal_surat'},
                    {data: 'sifat'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
