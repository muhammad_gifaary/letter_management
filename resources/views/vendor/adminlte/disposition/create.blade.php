@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <section class="content-header">
              <h1>
                Data Surat Disposisi
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-building"></i> Surat Disposisi</a></li>
                <li><a href="#" class="active">Tambah Surat Disposisi</a></li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Tambah Surat Disposisi</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::open(['route' => 'disposition', 'method' => 'post','files'=>true]) !!}
                            @include('adminlte::disposition._form')
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </section>
        </div>
    </div>
</div>

@endsection
