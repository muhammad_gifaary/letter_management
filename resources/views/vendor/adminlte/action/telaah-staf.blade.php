<a href="{{ route('telaah-staf.edit',$id) }}" class="btn  btn-primary btn-flat btn-sm" ><span class="glyphicon glyphicon-edit"></span></a>
<a href="{{ route('telaah-staf.delete',$id) }}" class="btn  btn-danger btn-flat btn-sm" ><span class="glyphicon glyphicon-remove"></span></a>
<a href="{{ route('telaah-staf.show',$id) }}" target="_blank" class="btn  btn-info btn-flat btn-sm" title="print" ><span class="glyphicon glyphicon-print"></span></a>
{{--<a href="{{ route('telaah-staf.pdf',$id) }}" class="btn  btn-success btn-flat btn-sm" title="PDF" ><span class="glyphicon glyphicon-download"></span></a>--}}

