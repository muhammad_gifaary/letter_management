@if(!$child)
    <a href="{{ route('spt.edit',$id) }}" class="btn  btn-primary btn-flat btn-sm" ><span class="glyphicon glyphicon-edit"></span></a>
@endif
<a href="{{ route('spt.delete',$id) }}" class="btn  btn-danger btn-flat btn-sm" ><span class="glyphicon glyphicon-remove"></span></a>
<a href="{{ route('spt.show',$id) }}" target="_blank" class="btn  btn-info btn-flat btn-sm" title="print" ><span class="glyphicon glyphicon-print"></span></a>
@if(!$child)
<a href="{{ route('sppd.create',$id) }}" target="_blank" class="btn  btn-success btn-flat btn-sm" title="SPPD" ><span class="glyphicon glyphicon-file"></span></a>
@endif
<a href="{{ route('sppd.show',$id) }}" target="_blank" class="btn  btn-info btn-flat btn-sm" title="print SPPD" ><span class="glyphicon glyphicon-print"></span></a>
{{--<a href="{{ route('spt.pdf',$id) }}" class="btn  btn-success btn-flat btn-sm" title="PDF" ><span class="glyphicon glyphicon-download"></span></a>--}}

