<a href="{{ route('permission.edit',$id) }}" class="btn  btn-primary btn-flat btn-sm" ><span class="glyphicon glyphicon-edit"></span></a>
<a href="{{ route('permission.delete',$id) }}" class="btn  btn-danger btn-flat btn-sm" ><span class="glyphicon glyphicon-remove"></span></a>
