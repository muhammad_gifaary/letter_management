<a href="{{ route('disposition.edit',$id) }}" class="btn  btn-primary btn-flat btn-sm" ><span class="glyphicon glyphicon-edit"></span></a>
<a href="{{ route('disposition.delete',$id) }}" class="btn  btn-danger btn-flat btn-sm" ><span class="glyphicon glyphicon-remove"></span></a>
<a href="{{ route('disposition.show',$id) }}" target="_blank" class="btn  btn-info btn-flat btn-sm" title="print" ><span class="glyphicon glyphicon-print"></span></a>
{{--<a href="{{ route('disposition.pdf',$id) }}" class="btn  btn-success btn-flat btn-sm" title="PDF" ><span class="glyphicon glyphicon-download"></span></a>--}}
<a href="{{ route('disposition.detail',$id) }}" class="btn  btn-info btn-flat btn-sm" title="PDF" ><span class="glyphicon glyphicon-eye-open"></span></a>
