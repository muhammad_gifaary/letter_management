<a href="{{ route('user.edit',$id) }}" class="btn  btn-primary btn-flat btn-sm" ><span class="glyphicon glyphicon-edit"></span></a>
<a href="{{ route('user.delete',$id) }}" class="btn  btn-danger btn-flat btn-sm" ><span class="glyphicon glyphicon-remove"></span></a>
