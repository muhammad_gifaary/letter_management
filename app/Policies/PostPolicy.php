<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/15/18
 * Time: 4:49 PM
 */

namespace App\Policies;


use App\User;

class PostPolicy
{
    //use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return true;
    }

}
