<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/22/18
 * Time: 3:26 PM
 */

namespace App\Helpers;


use Carbon\Carbon;

class Letter
{
    public static function convertDateToReadableFormat($value)
    {
        return date('D, j M y',strtotime($value));
    }

    public static function diffDay($start_date, $end_date)
    {
        $start = Carbon::parse($start_date);
        $end = Carbon::parse($end_date);
        $length = $start->diffInDays($end);
        return $length+1;
    }
}
