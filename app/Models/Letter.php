<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/15/18
 * Time: 10:46 AM
 */

namespace App\Models;
use App\Http\Tratis\UuidTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Letter
 * @package App\Models
 */
class Letter extends BaseModel
{
    use UuidTrait;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $dates = ['created_at', 'updated_at', 'tanggal_surat','tanggal_diterima','deleted_at'];
    protected $casts = [
        'tanggal_surat' => 'date:Y-m-d',
        'tanggal_diterima' => 'date:Y-m-d',
        'diterusakan_kepada'=> 'array'
    ];

    public function employees()
    {
        return $this->hasMany(LetterEmployees::class);
    }

    public function child()
    {
        return $this->hasMany(Letter::class,'parent_id','id');
    }

    public function employeeSppd()
    {
        return $this->belongsTo(LetterEmployees::class,'employee_id_sppd');
    }

    /**
     * @param Builder $query
     * @return Builder|static
     */
    public function scopeDisposition(Builder $query)
    {
        return $query->where('type','disposition');
    }

    /**
     * @param Builder $query
     * @return Builder|static
     */
    public function scopeSptAndSppd(Builder $query)
    {
        return $query->where('type','spt')->orWhere('sppd');
    }

    /**
     * @param Builder $query
     * @return Builder|static
     */
    public function scopeSpt(Builder $query)
    {
        return $query->where('type','spt');
    }

    /**
     * @param Builder $query
     * @return Builder|static
     */
    public function scopeSppd(Builder $query)
    {
        return $query->where('type','sppd');
    }

    /**
     * @param Builder $query
     * @return Builder|static
     */
    public function scopeTelaah(Builder $query)
    {
        return $query->where('type','telaah');
    }


    /**
     * @return array
     */
    public static function getListSifatSurat()
    {
        return array(
            'sangat_segera'=>'Sangat Segera',
            'segera'=> 'Segera',
            'rahasia'=> 'Rahasia',
            'biasa'=> 'Biasa'
        );
    }

    public static function getListDiteruskanKepada()
    {
        return array(
            'sekretaris'=>'Sekretaris',
            'kabid_bina_marga'=> 'Kabid Bina Marga',
            'kabid_cipta_karya'=> 'Kabid Cipta Karya',
            'kabid_sumber_daya_air'=> 'Kabid Sumber Daya Air',
            'kabid_tata_ruang_dan_pertahanan'=> 'Kabid Tata Ruang Dan Pertahanan',
            'kabid_perumahan'=> 'Kabid Perumahan',
            'kabid_bina_jasa_kontruksi'=> 'Kabid Bina Jasa Kontruksi',
            'kasubbag_perencanaan'=> 'Kasubbag Perencanaan',
        );
    }

    public static function getListDenganHormat()
    {
        return ['Untuk Diketahui','Untuk Dipergunakan','Ikuti Perkembangangannya','Untuk diselesaikan dan dilaporkan','Minta Penjelasan/Saran','Untuk dihadiri','Diteruskan kepada'];
    }

    public function getSifatAttribute($value)
    {
        if($value){
            $data = self::getListSifatSurat();
            return $data[$value];
        }
    }

    public function getDenganHormat($value)
    {
        if($value){
            $data = self::getListDenganHormat();
            return $data[$value];
        }
    }
}
