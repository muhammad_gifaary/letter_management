<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/15/18
 * Time: 10:47 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LetterEmployees extends BaseModel
{
    public function letter()
    {
        return $this->belongsTo(Letter::class,'letter_id');
    }
}
