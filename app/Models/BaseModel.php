<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/17/18
 * Time: 3:07 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected $guarded = ['_token','id'];
}
