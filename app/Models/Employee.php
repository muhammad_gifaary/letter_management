<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/15/18
 * Time: 10:41 AM
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Employee extends BaseModel
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
