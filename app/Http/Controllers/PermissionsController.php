<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/15/18
 * Time: 2:29 PM
 */

namespace App\Http\Controllers;


use App\Models\Permission;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Class PermissionsController
 * @package App\Http\Controllers
 */
class PermissionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('user');
        return view('adminlte::permission.index');

    }

    /**
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Datatables $datatables){
        $query = Permission::get();

        return $datatables->collection($query)
            ->addColumn('action', 'adminlte::action.permission')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminlte::permission.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:128'
        ]);

        $permission = Permission::create($request->all());
        //flash('Success store categories to database', 'success');
        return redirect()->route('permission');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('adminlte::permission.edit',compact("permission"));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:128'
        ]);
        $permission->update($request->all());
        return redirect()->route('permission');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        return redirect('permission');
    }
}
