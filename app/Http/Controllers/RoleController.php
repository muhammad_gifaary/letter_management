<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/15/18
 * Time: 12:40 PM
 */

namespace App\Http\Controllers;


use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Class RoleController
 * @package App\Http\Controllers
 */
class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('user');
        $roles = Role::orderBy('name','asc')->get();
        $permissions = Permission::all();
        $akses = array(
            '0' => 'No',
            '1' => 'Yes'
        );
        return view('adminlte::role.index',compact('roles','permissions','akses'));

    }

    /**
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Datatables $datatables){
        $query = Role::get();

        return $datatables->collection($query)
            ->addColumn('action', 'adminlte::action.role')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['permission'] =array();
        $permission = Permission::orderBy('name', 'asc')->get();
        return view('adminlte::role.create',compact('permission','data'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string|max:50'
        ]);
        $data['name'] = $request->get('name');
        $role = Role::create($data);

        // $permission =  $request->input("permission");
        // if($permission){
        //     foreach ($permission as $p) {
        //         $role->addPermission($p);
        //     }
        // }


        //flash('Success store categories to database', 'success');
        return redirect()->route('role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $data['permission'] =array();
        foreach ($role->permissions as $per) {
            $data['permission'][$per->name]=$per->name;
        }
        //print_r($data['permission']);
        $permission = Permission::orderBy('name', 'asc')->get();
        return view('adminlte::role.edit',compact("role","permission","data"));
    }


    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $active = $request->get("active");
        if($active==1){
            //add permission
            $role->addPermission($request->get("permission_name"));
        }else{
            //remove permission
            $role->removePermission($request->get("permission_name"));
        }
        $data['sukes'] = "sukses update";
        echo json_encode($data);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return redirect('role');
    }
}
