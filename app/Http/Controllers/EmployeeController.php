<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/17/18
 * Time: 12:22 PM
 */

namespace App\Http\Controllers;


use App\Models\Employee;
use App\Models\Grade;
use App\Models\Position;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

/**
 * Class EmployeeController
 * @package App\Http\Controllers
 */
class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('employee');
        return view('adminlte::employee.index');
    }

    /**
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Datatables $datatables){
        $query = Employee::all();

        return $datatables->collection($query)
            ->addColumn('action', 'adminlte::action.employee')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminlte::employee.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'nip' => 'required',
            'position' => 'required',
            'grade'     => 'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        Employee::create($request->except(['position_id','grade_id','_token']));

        Position::updateOrCreate(['name'=>$request->position]);
        Grade::updateOrCreate(['name'=>$request->grade]);

        return redirect()->route('employee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Employee::findOrFail($id);

        return view('adminlte::employee.edit',compact("model"));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);
        $this->validate($request, [
            'name' => 'required',
            'nip' => 'required',
            'position' => 'required',
            'grade'     => 'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $employee->update($request->except(['position_id','grade_id','_token']));

        if($employee->user_id!=null){
            $user = User::find($employee->user_id);
            $user->name = $request->name;
            $user->save();
        }

        Position::updateOrCreate(['name'=>$request->position]);
        Grade::updateOrCreate(['name'=>$request->grade]);


        return redirect()->route('employee');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $karyawan = Employee::find($id);
        $karyawan->user()->delete();
        $karyawan->delete();
        return redirect('employee');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListEmployee(Request $request)
    {
        $term = $request->term;
        $data = Employee::where("name","like",'%'.$term.'%')->take(10)->get();

        $results =[];
        foreach ($data as $key => $v) {
            $results[]=['id'=>$v->id,'value'=>$v->name." (".$v->position.")",'text'=>$v->name." (".$v->position.")"];
        }

        return response()->json($results);
    }
}
