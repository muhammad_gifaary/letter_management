<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Letter;
use App\Models\LetterEmployees;
use App\Models\Setting;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use LetterHelper;
use Yajra\DataTables\DataTables;

class SptController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('spt_sppd');
        return view('adminlte::spt.index');
    }


    public function data(DataTables $datatables)
    {
        $query = Letter::with(['child','employees'])->Spt()->orderBy('created_at','DESC')->select('letters.*');

        return $datatables->of($query)
            ->addColumn('action', 'adminlte::action.spt')
            ->editColumn('tgl_berangkat',function ($query){
                return LetterHelper::convertDateToReadableFormat($query->tgl_berangkat);
            })
            ->editColumn('tgl_kembali',function ($query){
                return LetterHelper::convertDateToReadableFormat($query->tgl_kembali);
            })
            ->addColumn('status_child',function($query){
                if($query->child()){
                    return true;
                }else{
                    return false;
                }
            })
            ->addColumn('employee',function ($query){
                $emp ='';
                //Log::error('employee',$query->em)
                foreach ($query->employees as $employee)
                {
                    $emp.=" ".$employee->name."\n";
                }

                return $emp;
            })
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = array();
        return view('adminlte::spt.create',compact('employee'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'letter_no' => 'required',
            'tujuan' => 'required',
            'tgl_berangkat'     => 'required',
            'tgl_kembali'     => 'required',
            'ditetapkan_di'=> 'required',
            'tgl_penetapan'=> 'required',
            'maksud_perjalanan_dinas'=>'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $data = $request->except(['employees']);
        $data['type']='spt';
        $letter = Letter::create($data);

        foreach ($request->employees as $employee){
            $emp = Employee::find($employee);
            if($emp){
                $letterEmployee = new LetterEmployees([
                    'employee_id'=>$employee,
                    'name' => $emp->name,
                    'nip'   => $emp->nip,
                    'position'=> $emp->position,
                    'grade'=> $emp->grade
                ]);
                $letter->employees()->save($letterEmployee);
            }
        }

        return redirect()->route('sppd.create',['id'=>$letter->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $letter = Letter::findOrFail($id);
        $setting = Setting::where('code','sekretaris')->first();
        $sekretaris = Employee::find($setting->value2);

        return view('adminlte::spt.print',compact('letter','sekretaris'));
    }

    public function downloadPdf($id)
    {
        $data['letter'] = Letter::findOrFail($id);
        $setting = Setting::where('code','sekretaris')->first();

        $sekretaris = Employee::find($setting->value2);
        $data['sekretaris'] = $sekretaris;

        $pdf = PDF::loadView('adminlte::telaah-staf.pdf',$data);
//        $pdf->save(storage_path().'surat_'.$letter->letter_no.'.pdf');
        return $pdf->download('surat_'.$data['letter']->letter_no.'.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Letter::find($id);
        $data = Employee::whereIn('id',$model->employees()->pluck('employee_id'))->get();
        $employee =[];
        foreach ($data as $key => $v) {
            $employee[]=['id'=>$v->id,'text'=>$v->name." (".$v->position.")"];
        }

        return view('adminlte::spt.edit',compact("model","employee"));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'letter_no' => 'required',
            'tujuan' => 'required',
            'tgl_berangkat'     => 'required',
            'tgl_kembali'     => 'required',
            'ditetapkan_di'=> 'required',
            'tgl_penetapan'=> 'required',
            'maksud_perjalanan_dinas'=>'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $letter = Letter::find($id);
        $letter->employees()->delete();

        $data = $request->except(['employees']);
        $data['type']='spt';
        $letter->update($data);

        foreach ($request->employees as $employee){
            $emp = Employee::find($employee);
            if($emp){
                $letterEmployee = new LetterEmployees([
                    'employee_id'=>$employee,
                    'name' => $emp->name,
                    'nip'   => $emp->nip,
                    'position'=> $emp->position,
                    'grade'=> $emp->grade
                ]);
                $letter->employees()->save($letterEmployee);
            }
        }

        return redirect()->route('spt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $letter = Letter::findOrFail($id);
        $letter->child()->delete();
        $letter->employees()->delete();
        $letter->delete();

        return redirect('spt');
    }
}
