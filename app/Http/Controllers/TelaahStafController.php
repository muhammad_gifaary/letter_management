<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 11/1/18
 * Time: 4:37 PM
 */

namespace App\Http\Controllers;


use App\Models\Employee;
use App\Models\Letter;
use App\Models\Setting;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use LetterHelper;

class TelaahStafController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('disposition');
        return view('adminlte::telaah-staf.index');
    }

    /**
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Datatables $datatables){
        $query = Letter::telaah()->orderBy('created_at','DESC')->get();

        return $datatables->collection($query)
            ->addColumn('action', 'adminlte::action.telaah-staf')
            ->editColumn('tanggal_surat',function ($query){
                return LetterHelper::convertDateToReadableFormat($query->tanggal_surat);
            })
            ->editColumn('persetujuan',function ($query){
                if($query->persetujuan){
                    return 'Disetujui';
                }else{
                    return 'Tidak setuju';
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminlte::telaah-staf.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'kepada' => 'required',
            'dari' => 'required',
            'tanggal_surat' => 'required',
            'letter_no'     => 'required',
            'perihal'     => 'required',
            'persetujuan'=> 'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $data = $request->all();
        $data['type']='telaah';


        $letter = Letter::create($data);



        return redirect()->route('telaah-staf');
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $letter = Letter::findOrFail($id);
        $setting = Setting::where('code','sekretaris')->first();

        $sekretaris = Employee::find($setting->value2);

        return view('adminlte::telaah-staf.print',compact('letter','sekretaris'));
    }

    public function detail($id)
    {
        $letter = Letter::findOrFail($id);

        return view('adminlte::telaah-staf.detail',compact('letter'));
    }


    public function downloadPdf($id)
    {
        $data['letter'] = Letter::findOrFail($id);
        $setting = Setting::where('code','sekretaris')->first();

        $sekretaris = Employee::find($setting->value2);
        $data['sekretaris'] = $sekretaris;

        $pdf = PDF::loadView('adminlte::telaah-staf.pdf',$data);
//        $pdf->save(storage_path().'surat_'.$letter->letter_no.'.pdf');
        return $pdf->download('surat_'.$data['letter']->letter_no.'.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Letter::findOrFail($id);

        return view('adminlte::telaah-staf.edit',compact("model"));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'kepada' => 'required',
            'dari' => 'required',
            'tanggal_surat' => 'required',
            'letter_no'     => 'required',
            'perihal'     => 'required',
            'persetujuan'=> 'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $letter = Letter::findOrFail($id);
        $letter->update($request->all());



        return redirect()->route('telaah-staf');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $letter = Letter::findOrFail($id);
        $letter->delete();

        return redirect('telaah-staf');
    }
}
