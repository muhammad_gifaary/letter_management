<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Letter;
use App\Models\Setting;
use Illuminate\Http\Request;

class SppdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if($id){
            $letter = Letter::findOrFail($id);
            $employee = $letter->employees()->get();
            return view('adminlte::sppd.create',compact('letter','employee'));

        }else{
            return redirect('sppd');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$letter_id)
    {
        $letter = Letter::find($letter_id);

        if($letter->type=='spt'){
            $employees = $letter->employees()->get();
            foreach ($employees as $employee ){
                $data = $request->data[$employee->id];
                $data['employee_id_sppd'] = $employee->id;
                $data['parent_id'] = $letter_id;
                $data['type'] ='sppd';
                Letter::create($data);
            }
            return redirect('spt');
        }else{
            return redirect('spt');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $letters = Letter::find($id);
        $setting = Setting::where('code','sekretaris')->first();
        $sekretaris = Employee::find($setting->value2);

        return view('adminlte::sppd.print',compact('sekretaris','letters'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
