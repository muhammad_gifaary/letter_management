<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/21/18
 * Time: 3:12 PM
 */

namespace App\Http\Controllers;
use App\Mail\Disposition;
use App\Mail\NotifDisposition;
use App\Mail\NotifResponse;
use App\Models\Employee;
use App\Models\Letter;
use App\Models\LetterEmployees;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;
use Barryvdh\DomPDF\Facade as PDF;
use LetterHelper;


/**
 * Class DispositionController
 * @package App\Http\Controllers
 */
class DispositionController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('disposition');
        return view('adminlte::disposition.index');
    }

    /**
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Datatables $datatables){
        $query = Letter::disposition()->orderBy('created_at','DESC')->get();

        return $datatables->collection($query)
            ->addColumn('action', 'adminlte::action.disposition')
            ->editColumn('tanggal_surat',function ($query){
                return LetterHelper::convertDateToReadableFormat($query->tanggal_surat);
            })
            ->editColumn('tanggal_diterima',function ($query){
                return LetterHelper::convertDateToReadableFormat($query->tanggal_diterima);
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sifat_surat = Letter::getListSifatSurat();
        $terusan = Letter::getListDiteruskanKepada();
        $dengan_hormat = Letter::getListDenganHormat();
        return view('adminlte::disposition.create',compact('sifat_surat','terusan','dengan_hormat'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'dari' => 'required',
            'letter_no' => 'required',
            'tanggal_surat' => 'required',
            'tanggal_diterima'     => 'required',
            'no_agenda'     => 'required',
            'perihal'     => 'required',
            'attachment'=> 'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $data = $request->except('attachment');
        $data['type']='disposition';

        $pathLogo='';
        if ($request->hasFile('attachment')) {
            $pathLogo = $request->file('attachment')->storeAs('attachment',$request->letter_no.".". $request->file('attachment')->getExtension(),'uploads');
        }

        $data['attachment'] = $pathLogo;

        $letter = Letter::create($data);

        //jika yang login beda denga id sekretaris maka notif ke sekretaris

        $sekre = Setting::where('code','sekretaris')->first();

        if($sekre->value2!=session('employee_id',0)){
            try{
                $employee = Employee::find($sekre->value2);
                Mail::to($employee->email)->send(new Disposition($letter));
            }catch (\Exception $e){
                Log::error('mail store response'.$e->getMessage());
            }
        }


        return redirect()->route('disposition');
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $letter = Letter::findOrFail($id);
        //$sifat_surat = Letter::getListSifatSurat();
        $terusan = Letter::getListDiteruskanKepada();
        $dengan_hormat = Letter::getListDenganHormat();

        return view('adminlte::disposition.print',compact('letter','terusan','dengan_hormat'));
    }

    public function detail($id)
    {
        $letter = Letter::findOrFail($id);
        $terusan = Letter::getListDiteruskanKepada();
        $dengan_hormat = Letter::getListDenganHormat();

        return view('adminlte::disposition.detail',compact('letter','terusan','dengan_hormat'));
    }

    public function responseLetter($id)
    {
        $letter = Letter::findOrFail($id);
        $letterEmployee = LetterEmployees::where(['letter_id'=>$id,'employee_id'=>session('employee_id',0)])->first();
        if($letterEmployee)
        {
            $terusan = Letter::getListDiteruskanKepada();
            $dengan_hormat = Letter::getListDenganHormat();

            return view('adminlte::disposition.response',compact('letter','terusan','dengan_hormat','letterEmployee'));
        }else{
            return redirect()->route('login');
        }

    }

    public function storeResponse(Request $request,$id)
    {
        if($request->status_accept=='0') {
            $this->validate($request, [
                'reason' => 'required'
            ],[
                'required' => 'Wajib di isi'
            ]);
        }

        try{
            $letterEmployee = LetterEmployees::where(['letter_id'=>$id,'employee_id'=>session('employee_id',0)])->first();
            $letterEmployee->update($request->all());

            $sekre = Setting::where('code','sekretaris')->first();
            $employee = Employee::find($sekre->value2);

            Mail::to($employee->email)->sendNow(new NotifResponse($letterEmployee));
        }catch (\Exception $e){
            Log::error('mail store response'.$e->getMessage());
        }

        return redirect()->route('login');
    }

    public function downloadPdf($id)
    {
        $data['letter'] = Letter::findOrFail($id);
        $data['sifat_surat'] = Letter::getListSifatSurat();
        $data['terusan'] = Letter::getListDiteruskanKepada();
        $data['dengan_hormat'] = Letter::getListDenganHormat();

        $pdf = PDF::loadView('adminlte::disposition.pdf',$data);
//        $pdf->save(storage_path().'surat_'.$letter->letter_no.'.pdf');
        return $pdf->download('surat_'.$data['letter']->letter_no.'.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Letter::findOrFail($id);
        $sifat_surat = Letter::getListSifatSurat();
        $terusan = Letter::getListDiteruskanKepada();
        $dengan_hormat = Letter::getListDenganHormat();


        return view('adminlte::disposition.edit',compact("model","sifat_surat","terusan","dengan_hormat"));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'dari' => 'required',
            'letter_no' => 'required',
            'tanggal_surat' => 'required',
            'tanggal_diterima'     => 'required',
            'no_agenda'     => 'required',
            'perihal'     => 'required',
            'diterusakan_kepada.0'=>'required'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $letter = Letter::findOrFail($id);
        $letter->update($request->all());

        $sekre = Setting::where('code','sekretaris')->first();

        if($sekre->value2==session('employee_id',0)){
            // kiirm disposisi ke pegawai
            $diterusakan_kepada = $request->input('diterusakan_kepada');
            $email = [];
            foreach ($diterusakan_kepada as $key=>$value)
            {
                $setting = Setting::where('code',$value)->first();
                $employee = Employee::find($setting->value2);
                if($employee){
                    $letter_employee = new LetterEmployees([
                        'employee_id' => $employee->id,
                        'name'        => $employee->name,
                        'nip'          => $employee->nip,
                        'position'      => $employee->position,
                        'grade'     => $employee->grade,
                        'status_accept' => null
                    ]);

                    $letter->employees()->save($letter_employee);

                    if(!empty($employee->email))
                    {
                        array_push($email,$employee->email);
                    }
                }
            }
            if(sizeof($email)>0)
            {
                try{
                    Mail::to($email)->send(new NotifDisposition($letter));
                }catch (\Exception $e){
                    Log::error('error notif disposisi '.$e->getMessage());
                }
            }
        }

        return redirect()->route('disposition');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $letter = Letter::findOrFail($id);
        $letter->delete();

        return redirect('disposition');
    }

}
