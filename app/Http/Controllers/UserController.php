<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Role;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;
use App\Mail\NewUser;

/*
    author          : muhammad gifary (muhammadgifary@gmail.com)
    created date    : 5 Mei 2017
    last edited     : 5 Mei 2017
*/

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('user');
        return view('adminlte::user.index');
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Datatables $datatables){
        $query = User::with(['roles'])->get();

        return $datatables->collection($query)
            ->addColumn('action', 'adminlte::action.user')
            ->addColumn('roles', function ($query) {
                return $query->roles()->first()->name;

            })

            ->addColumn('nip', function ($query) {
                if($query->employee){
                    return $query->employee->nip;
                }else{
                    return '-';
                }
            })


            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $role         = Role::pluck("name","name");
        $collection     = Employee::whereNull('user_id')->get(["name","id","position","grade"]);
        $karyawan = $collection->mapWithKeys(function ($karyawan) {
            return [$karyawan['id'] => $karyawan['name']. " (".$karyawan['position'].")"];
        });

        $karyawan->all();

        return view('adminlte::user.create',compact('role','karyawan'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'role_id' => 'required',
            'employee_id' => 'required',
            'password' => 'required|min:6',
            'email'     => 'required|unique:users,email'
        ],[
            'required' => 'Wajib di isi'
        ]);

        $karyawan = Employee::findOrFail($request->input('employee_id'));

        $data_user['name'] = $karyawan->name;
        $data_user['password'] = bcrypt($request->input('password'));
        $data_user['email'] = $request->input('email');
        $user = User::create($data_user);

        $user->assignRole($request->input('role_id'));

        $karyawan->email = $request->input('email');
        $karyawan->user_id = $user->id;
        $karyawan->save();

        //Mail::to($request->email)->send(new NewUser($user,$request->password));

        //flash('Success store categories to database', 'success');
        return redirect()->route('user');
    }


    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $role         = Role::pluck("name","name");

        return view('adminlte::user.edit',compact("user","role"));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'role_id' => 'required'
        ]);
        /*if(strcmp($request->username, $request->username_old)!=0){
            $this->validate($request, [
                'username' => 'required|unique:users,username',
            ]);
        }*/
        if(strcmp($request->email, $request->email_old)!=0){
            $this->validate($request, [
                'email'     => 'required|unique:users,email'
            ]);
        }
        $user->revokeRole($user->roles()->first()->name);
        if($request->has('password') && $request->input('password')!=''){
            $user->password = bcrypt($request->password);
            //Mail::to($request->email)->send(new NewUser($user,$request->password));
        }
        $user->email = $request->email;
        $user->save();

        if($request->has('password') && $request->input('password')!=''){
            //Mail::to($request->email)->send(new NewUser($user,$request->password));
        }
        $user->assignRole($request->role_id);

        $karyawan = Employee::where('user_id',$id)->first();
        $karyawan->email = $request->email;
        $karyawan->save();


        return redirect()->route('user');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $karyawan = Employee::where('user_id',$id)->first();
        if($karyawan){
            $karyawan->user_id = null;
            $karyawan->save();
        }

        $role = User::findOrFail($id);
        $role->delete();
        return redirect('user');
    }
}
