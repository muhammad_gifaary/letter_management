<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/17/18
 * Time: 3:28 PM
 */

namespace App\Http\Controllers;


use App\Models\Setting;
use Illuminate\Http\Request;

/**
 * Class SettingController
 * @package App\Http\Controllers
 */
class SettingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('setting');
        $settings = Setting::all();
        return view('adminlte::setting.index',compact('settings'));
    }

    /**
     * @param Request $request
     */
    public function store(Request $request){
        $setting = Setting::where('code',$request->code)->first();
        $setting->value1 = $request->value1;
        $setting->value2 = $request->value2;
        $setting->save();

        $data['sukes'] = "sukses update";
        echo json_encode($data);
    }
}
