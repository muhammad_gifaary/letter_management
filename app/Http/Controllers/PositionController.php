<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/17/18
 * Time: 12:40 PM
 */

namespace App\Http\Controllers;


use App\Models\Position;
use Illuminate\Http\Request;

/**
 * Class PositionController
 * @package App\Http\Controllers
 */
class PositionController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPosition(Request $request)
    {
        $term = $request->term;
        $data = Position::where("name","like",'%'.$term.'%')->take(10)->get();

        $results =[];
        foreach ($data as $key => $v) {
            $results[]=['id'=>$v->id,'value'=>$v->name];
        }

        return response()->json($results);
    }
}
