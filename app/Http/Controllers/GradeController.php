<?php
/**
 * Created by PhpStorm.
 * User: gifary
 * Date: 10/17/18
 * Time: 1:04 PM
 */

namespace App\Http\Controllers;


use App\Models\Grade;
use Illuminate\Http\Request;

/**
 * Class GradeController
 * @package App\Http\Controllers
 */
class GradeController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDegree(Request $request)
    {
        $term = $request->term;
        $data = Grade::where("name","like",'%'.$term.'%')->take(10)->get();

        $results =[];
        foreach ($data as $key => $v) {
            $results[]=['id'=>$v->id,'value'=>$v->name];
        }

        return response()->json($results);
    }
}
