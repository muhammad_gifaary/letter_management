<?php

namespace App\Mail;

use App\Models\Letter;
use App\Models\LetterEmployees;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifResponse extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var LetterEmployees
     */
    public $letterEmployee;
    /**
     * Create a new message instance.
     *
     * @param LetterEmployees $letterEmployee
     */
    public function __construct(LetterEmployees $letterEmployee)
    {
        $this->letterEmployee =  $letterEmployee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.disposition.notifresponse');
    }
}
