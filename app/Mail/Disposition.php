<?php

namespace App\Mail;

use App\Models\Letter;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Disposition extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * @var Letter
     */
    public $letter;
    /**
     * Create a new message instance.
     *
     * @param Letter $letter
     */
    public function __construct(Letter $letter)
    {
        $this->letter =  $letter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url_edit_disposisi = url('/disposition/edit/'.$this->letter->id);
        return $this->markdown('emails.disposition.diteruskan')->with([
            'url_edit'=>$url_edit_disposisi
        ]);
    }
}
